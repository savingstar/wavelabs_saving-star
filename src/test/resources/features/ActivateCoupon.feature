Feature: Coupon Activation feature
Descripton: Coupon Activation Scenarios 

@Reg
  Scenario Outline: Sign up as a new user and activate the coupon by click on plus button of the product
    Given a user is at savingstar home page
    When user tries to activate rebate using Rebate link
	And user starts signup process by using the join now to get rebate button from the Rebate pop up
	And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
	And Click on may be later link
    Then Click on plus button of the product in coupons page
  
    Examples: 
      | firstName | lastName | zipCode | password |
      | Admin     | user     | 02451   | password |
      
@Reg
  Scenario Outline: Sign up as a new user and activate the coupon from Offer link 
    Given a user is at savingstar home page
    When user tries to activate rebate using Rebate link
	And user starts signup process by using the join now to get rebate button from the Rebate pop up
	And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
	And Click on may be later link
    Then Coupon should be activated for the product
  
    Examples: 
      | firstName | lastName | zipCode | password |
      | Admin     | user     | 02451   | password |
      
@Reg
Scenario Outline: signup as a new user and activate the coupon by click on image and activate the rebate button
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And Click on may be later link
      Then Click on the product in coupons page
      And Click on activate the rebate button and verify the rebate button is activated
  
    Examples: 
      | firstName | lastName | zipCode | password |
      | Admin     | user     | 02451   | password | 
      
@Reg
Scenario Outline: Activate more than one coupons for the new user and verify the count in activate tab
	Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And Click on may be later link
      Then Activate the coupons for the new user
      And Navigateto activate tab 
      And Verify the count of activated coupons in activation tab
	
	Examples: 
      | firstName | lastName | zipCode | password |
      | Admin     | user     | 02451   | password |
 
@Reg
Scenario Outline: Activate the coupons in two different ways for the new user
	Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And Click on may be later link
      Then Click on plus button of the product in coupons page
      And Click on the product in coupons page
   	  And Click on activate the rebate button and verify the rebate button is activated
   	  
   	  Examples: 
      | firstName | lastName | zipCode | password |
      | Saving     | star     | 02451   | password |
 
@Reg     
Scenario Outline: Activate the coupons for the user which is already activated
	Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And Click on may be later link
      And Click on the product in coupons page
      And Click on activate the rebate button and verify the rebate button is activated
      And Close hide message and click on the signout button
      And user starts signin process by using the Sign In button
      And enter email address as generated from the above step 
      And enter password as "<password>" on Sign Page
      And submits the request for Sign In      
   	  And Click on the product in coupons page which is already activated
   	  Then Check the activate the rebate button for the product
   	  
   	  Examples: 
      | firstName | lastName | zipCode | password  | 
      | Saving     | star     | 02451   | password |
      
@Reg
  Scenario Outline: Activate the coupon from Offer link which is already activated
   Given a user is at savingstar home page
    When user tries to activate rebate using Rebate link
	And user starts signup process by using the join now to get rebate button from the Rebate pop up
	And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
	And Click on may be later link
    And Click on signout button
    And user tries to activate rebate using Rebate link
    And Click on sign in button
    And enter email address as generated from the above step 
    And enter password as "<password>" on Sign Page
    And submits the request for Sign In      
	Then Check the activation status of the product
  
    Examples: 
      | firstName | lastName | zipCode | password  | 
      | Saving     | star     | 02451   | password |
 
@Reg
  Scenario Outline: Activate the multiple coupons in different ways and try to activate the same coupons
   Given a user is at savingstar home page
    When user starts the signup process using Join Now 
	And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
	And Click on may be later link
	And Click on plus button of the product in coupons page
    And Click on the product in coupons page
   	And Click on activate the rebate button and verify the rebate button is activated
   	And Close hide message and click on the signout button
    And user starts signin process by using the Sign In button
    And enter email address as generated from the above step 
    And enter password as "<password>" on Sign Page
    And submits the request for Sign In 
    Then Click on the first product
    And Check the activation status of the product
    And click on the second product     
    And Check the activation status of the product
	
	
	Examples: 
      | firstName | lastName | zipCode | password  | 
      | Saving     | star     | 02451   | password |

@Reg
Scenario Outline: Activate more than one coupons for the new user and verify the activated coupon in activate tab
	Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And Click on may be later link
      And Click on plus button of the product in coupons page
      And Get the text of the activated coupon
      And Navigateto activate tab
      Then Verify the activated coupon in activate tab
	
	Examples: 
      | firstName | lastName | zipCode | password |
      | Admin     | user     | 02451   | password |
