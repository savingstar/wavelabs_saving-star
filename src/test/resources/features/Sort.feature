Feature: Sorting Feature covering all Scenarios
Descripton: Sorting Feature all Scenarios  

@Reg
  Scenario Outline: Sign up as a new user and sort the products by product name
      Given a user is at Savingstar Home page 
      When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And Click on may be later link
      And click on the sort by
      And Select sort by product name
      Then products should be sort in alphabetical order
      
       Examples: 
      | firstName | lastName | zipCode | password | 
      | Admin      | user     | 02451   | password | 
      
@Reg
  Scenario Outline: Sign up as a new user and sort the products by Expiration
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And Click on may be later link
      And click on the sort by
      And Select sort by expiration
      Then check the order of the products by expiration date
      
       Examples: 
      | firstName | lastName | zipCode | password | 
      | Admin      | user     | 02451   | password | 
      
@Reg
  Scenario Outline: Sign up as a new user and sort the products by reward amount
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And Click on may be later link
      And click on the sort by
      And Select sort by Reward amount
      Then the reward amount of the products in decending order
      
       Examples: 
      | firstName | lastName | zipCode | password | 
      | Admin      | user     | 02451   | password | 
      
@Reg
  Scenario Outline: Sign up as a new user and sort the products by newest
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And Click on may be later link
      And click on the sort by
      And Select sort by Newest
      Then check the order of the products by start date
      
       Examples: 
      | firstName | lastName | zipCode | password | 
      | Admin      | user     | 02451   | password | 
      
@Reg
  Scenario Outline: Sign up as a new user and sort the products by Featured
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And Click on may be later link
      And click on the sort by
      And Select sort by featured
      Then check the order of the products by featured
      
       Examples: 
      | firstName | lastName | zipCode | password | 
      | Admin      | user     | 02451   | password | 
      
@Reg
  Scenario Outline: Sign up as a new user and check the default sorting order
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And Click on may be later link
      Then check the default sorting order
      
       Examples: 
      | firstName | lastName | zipCode | password | 
      | Admin      | user     | 02451   | password | 