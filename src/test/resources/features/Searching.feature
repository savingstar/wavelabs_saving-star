Feature: Sorting Feature covering all Scenarios 
	Descripton: Sorting Feature all Scenarios 

@Reg 
Scenario Outline: Sign up as a new user and search with type of offer like one or many 
	Given a user is at Savingstar Home page 
	When user starts the signup process using Join Now 
	And enter firstName as "<firstName>" 
	And enter lastName as "<lastName>" 
	And enter zip code as "<zipCode>" 
	And generate unique email Id 
	And enter email address as generated from the above step 
	And enter password as "<password>" 
	And submits the request for SignUp 
	And Click on may be later link 
	And Click on search bar
	And enter text in the search box as "<text>"
	Then only the products which has one or many offer has to be display
	
	Examples: 
		| firstName | lastName | zipCode | password |    text     |
		| Admin     | user     | 02451   | password | one or many |

@Reg 
Scenario Outline: Sign up as a new user and search with product name
	Given a user is at Savingstar Home page 
	When user starts the signup process using Join Now 
	And enter firstName as "<firstName>" 
	And enter lastName as "<lastName>" 
	And enter zip code as "<zipCode>" 
	And generate unique email Id 
	And enter email address as generated from the above step 
	And enter password as "<password>" 
	And submits the request for SignUp 
	And Click on may be later link 
	And Click on search bar
	And Enter the product name in the search box as "<productName>"
	Then only the products which matches with the product name has to be display
	
	Examples: 
		| firstName | lastName | zipCode | password | productName         |
		| Admin     | user     | 02451   | password | Bounty paper towels |
		
@Reg 
Scenario Outline: Sign up as a new user and search with offer price 
	Given a user is at Savingstar Home page 
	When user starts the signup process using Join Now 
	And enter firstName as "<firstName>" 
	And enter lastName as "<lastName>" 
	And enter zip code as "<zipCode>" 
	And generate unique email Id 
	And enter email address as generated from the above step 
	And enter password as "<password>" 
	And submits the request for SignUp 
	And Click on may be later link 
	And Click on search bar
	And Enter the "<offrePrice>"
	Then only the products which matches offer price has to be display
	
	Examples: 
		| firstName | lastName | zipCode | password |  offrePrice |
		| Admin     | user     | 02451   | password |  $6         |
		
@Reg 
Scenario Outline: Sign up as a new user and search with spend money
	Given a user is at Savingstar Home page 
	When user starts the signup process using Join Now 
	And enter firstName as "<firstName>" 
	And enter lastName as "<lastName>" 
	And enter zip code as "<zipCode>" 
	And generate unique email Id 
	And enter email address as generated from the above step 
	And enter password as "<password>" 
	And submits the request for SignUp 
	And Click on may be later link 
	And Click on search bar
	And Enter the "<spendMoney>"
	Then only the products which matches spend money has to be display
	
	Examples: 
		| firstName | lastName | zipCode | password | spendMoney |
		| Admin     | user     | 02451   | password | $20        |
		
@Reg 
Scenario Outline: Sign up as a new user and search with random text
	Given a user is at Savingstar Home page 
	When user starts the signup process using Join Now 
	And enter firstName as "<firstName>" 
	And enter lastName as "<lastName>" 
	And enter zip code as "<zipCode>" 
	And generate unique email Id 
	And enter email address as generated from the above step 
	And enter password as "<password>" 
	And submits the request for SignUp 
	And Click on may be later link 
	And Click on search bar
	And Enter "<randomText>" 
	Then verify the message
	
	Examples: 
		| firstName | lastName | zipCode | password | randomText |
		| Admin     | user     | 02451   | password | ghfjh      |
		
@Reg 
Scenario Outline: Sign up as a new user and search with number of the buying products
	Given a user is at Savingstar Home page 
	When user starts the signup process using Join Now 
	And enter firstName as "<firstName>" 
	And enter lastName as "<lastName>" 
	And enter zip code as "<zipCode>" 
	And generate unique email Id 
	And enter email address as generated from the above step 
	And enter password as "<password>" 
	And submits the request for SignUp 
	And Click on may be later link 
	And Click on search bar
	And Enter "<buyingNumber>" of the products
	Then verify the message
	
	Examples: 
		| firstName | lastName | zipCode | password | buyingNumber |
		| Admin     | user     | 02451   | password | buy 5        |