#Author: tburmawala@savingstar.com
Feature: SignIn Feature covering all Scenarios
Descripton: SigIn Feature all Scenarios  

@Reg
  Scenario Outline: Sign in using Join Now button from homepage with valid credential
    Given a user is at savingstar home page
     When user starts signin process by using the Join Now button
      And user clicks on Sign In Link
      And enter email Address as "<email>" on Sign Page
      And enter password as "<password>" on Sign Page
      And submits the request for Sign In
     Then user sees the coupons page
  
    Examples: 
      | email                     | password | 
      | TESTUSER34@savingstar.com | letmein1 | 
  
  @Reg
  Scenario Outline: Sign in using Sign In button from homepage with valid credential
    Given a user is at savingstar home page
     When user starts signin process by using the Sign In button
      And enter email Address as "<email>" on Sign Page
      And enter password as "<password>" on Sign Page
      And submits the request for Sign In
     Then user sees the coupons page
  
    Examples: 
      | email                     | password | 
      | TESTUSER44@savingstar.com | letmein1 | 
  
  @Pending
  Scenario Outline: Sign in using Sign In with Facebook button from homepage with valid credential
    Given a user is at savingstar home page
     When user starts signin process by using the Sign In button
      And user uses Sign In with Facebook button
      And enter email Address as "<email>" on Sign Page
      And enter password as "<password>" on Sign Page
      And submits the fb request for Sign In
     Then user sees the coupons page
  
    Examples: 
      | email             | password      | 
      | qa@savingstar.com | Loyal2Coupons | 
  
  @Reg
  Scenario Outline: Sign in with invalid credential
    Given a user is at savingstar home page
     When user starts signin process by using the Sign In button
      And enter email Address as "<email>" on Sign Page
      And enter password as "<password>" on Sign Page
      And submits the request for Sign In
     Then user does not see the coupons page
      And error message "<message>" is displayed.
  
    Examples: 
      | email                     | password | message                                                                 | 
      | TESTUSER14@.com           | letmein1 | Unable to Log In because Invalid email address and password combination | 
      | usernotinsystem@ss.com    | letmein1 | Unable to Log In because Invalid email address and password combination | 
      |                           | letmein1 | Unable to Log In because Invalid email address and password combination | 
      | Testuser14@savingstar.com | let      | Unable to Log In because Invalid email address and password combination | 
      | Testuser14@savingstar.com |          | Unable to Log In because Invalid email address and password combination | 
      | T@savingstar.com          | letm     | Unable to Log In because Invalid email address and password combination | 
      |                           |          | Unable to Log In because Invalid email address and password combination | 
  
  @Reg
  Scenario Outline: Sign in from Forgot Password link in Sign In pop from home page with valid credential
    Given a user is at savingstar home page
     When user starts signin process by using the Sign In button
      And initiates password reset using Forgot Password link
      And user starts signin process by using the SignIn link from the Forgot Password pop up
      And enter email Address as "<email>" on Sign Page
      And enter password as "<password>" on Sign Page
      And submits the request for Sign In
     Then user sees the coupons page
  
    Examples: 
      | email                     | password | 
      | TESTUSER84@savingstar.com | letmein1 | 
  
  @Reg
  Scenario Outline: Sign in from Offer link with valid credential
    Given a user is at savingstar home page
     When user tries to activate rebate using Rebate link
      And user starts signin process by using the SignIn link from the Rebate pop up
      And enter email Address as "<email>" on Sign Page
      And enter password as "<password>" on Sign Page
      And submits the request for Sign In
  #Then user sees the coupons page
  	#  And Click on activate the rebate button
      Then the Rebate is Activated 
      And the account is created
  
    Examples: 
      | email                     | password | 
      | TESTUSER22@savingstar.com | letmein1 | 
  
  @Reg
  Scenario Outline: Sign in with valid credentials from Always On popup
    Given a user is at savingstar home page
     When user tries to enroll any Always On using Details button
      And user starts signin process by using the SignIn link from the Always On offer pop up
      And enter email Address as "<email>" on Sign Page
      And enter password as "<password>" on Sign Page
      And submits the request for Sign In
  #Then user sees the coupons page
     Then the Rebate is Activated 
      And the account is created
  
    Examples: 
      | email                     | password | 
      | TESTUSER62@savingstar.com | letmein1 | 
  
  @Reg
  Scenario Outline: Sign in with valid credentials from individual coupon page
    Given a user at single landing page by using coupon title "<couponTitle>"
     When user logins using Sign in Link
      And enter email Address as "<email>" on Sign Page
      And enter password as "<password>" on Sign Page
      And submits the request for Sign In
     Then user is logged in
      And user sees the individual coupon page.
  
    Examples: 
      | couponTitle          | email                     | password | 
      | Angel Soft® Products | TESTUSER92@savingstar.com | letmein1 | 
  
  @Reg
  Scenario Outline: Sign in using Log In from Branded experience page
    Given a user at branded experience page by using "<brandedExperienceName>"
     When user starts signin process by using the Log In button on BE page
      And enter email Address as "<email>" on BE page
      And enter password as "<password>" on BE page
      And submits the request on BE page
     Then user sees the Branded experience page.
  
    Examples: 
      | email                     | password | brandedExperienceName                | 
      | testuser55@savingstar.com | letmein1 | FRITO LAY BRANDED EXPERIENCE 8.11.16 | 
  
  @Reg
  Scenario Outline: Signin using Retailer Iframe using Login Link by giving valid creds
    Given a user at Iframe page by using "<iFrameName>"
     When a user starts Signin process by using the Log In button on Iframe page
      And enter email Address as "<email>" on  Iframe page
      And enter password as "<password>" on  Iframe page
      And submits the request on  Iframe page
     Then user sees the  Iframe page.
  
    Examples: 
      | email                     | password | iFrameName   | 
      | TESTUSER30@savingstar.com | letmein1 | Big Y iFrame | 
  
  
