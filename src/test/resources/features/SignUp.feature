#Author: Vivek (vivekb@wavelabs.in)
Feature: Signup Feaure covering all scenarios 
Descripton: Signup Feature all Scenarios  

@Reg
  Scenario Outline: Signup using Join Now Button with valid credentials 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user is on always-on page 
      And the account is created
  
    Examples: 
      | firstName | lastName | zipCode | password | 
      | test      | user     | 02451   | password | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving an existing email address & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId                   | password | message                                              | 
      | test      | user     | 02451   | TESTUSER25@savingstar.com | password | There is already an account with this email address. | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving all possible invalid zip codes to zip code field & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId          | password | message                                                                                         | 
      | test      | user     | 1safs   | zipuser1@xyz.com | password | Unable to create Account because Personally identifiable information is invalid, Zip is invalid | 
      | test      | user     | 3safs   | zipuser2@xyz.com | password | Unable to create Account because Personally identifiable information is invalid, Zip is invalid | 
      | test      | user     | 1234    | zipuser3@xyz.com | password | Unable to create Account because Personally identifiable information is invalid, Zip is invalid | 
      | test      | user     | 123     | zipuser4@xyz.com | password | Unable to create Account because Personally identifiable information is invalid, Zip is invalid | 
      | test      | user     | 12      | zipuser5@xyz.com | password | Unable to create Account because Personally identifiable information is invalid, Zip is invalid | 
      | test      | user     | 1       | zipuser6@xyz.com | password | Unable to create Account because Personally identifiable information is invalid, Zip is invalid | 
      | test      | user     | $%^&    | zipuser7@xyz.com | password | Unable to create Account because Personally identifiable information is invalid, Zip is invalid | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving email value which contains more than 128 characters & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId                                                                                                                           | password | message                                                              | 
      | test      | user     | 02451   | admin987654321admin987654321admin87654321admin987654321admin987654321admin987654321admin987654321admin987654321adminad@agmail.com | password | Unable to create Account because Email too long (128 characters max) | 
  @Reg
  Scenario Outline: Signup using Join Now Button by giving password value less than  8 characters & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId                   | password | message                                                                 | 
      | test      | user     | 02451   | passwordless@savewave.com | pass     | Unable to create Account because Password must be at least 8 characters | 
  @Reg
  Scenario Outline: Signup using Join Now Button by giving all fields as empty 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId | password | message                                            | 
      |           |          |         |         |          | Please include name, ZIP code, email and password. | 
  @Reg
  Scenario Outline: Signup using Join Now Button by giving first Name & last Name as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId                  | password | message                                            | 
      |           |          | 02451   | nameAsEmpty@savewave.com | password | Please include name, ZIP code, email and password. | 
  @Reg
  Scenario Outline: Signup using Join Now Button by giving Zip code as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId                 | password | message                                            | 
      | fist      | last     |         | zipAsEmpty@savewave.com | password | Please include name, ZIP code, email and password. | 
  @Reg
  Scenario Outline: Signup using Join Now Button by giving Email Id Value as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId | password | message                                               | 
      | fist      | last     | 02451   |         | password | Unable to create Account because Email can't be blank | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving  password value as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId                      | password | message                                                  | 
      | fist      | last     | 02451   | passwordAsEmpty@savewave.com |          | Unable to create Account because Password can't be blank | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving firstName,lastName and Zip code Values as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId                        | password | message                                            | 
      |           |          |         | nameAndZipAsEmpty@savewave.com | password | Please include name, ZIP code, email and password. | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving firstName,lastName and email Id values as empty & remaining all fields with valid details
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId | password | message                                            | 
      |           |          | 02451   |         | password | Please include name, ZIP code, email and password. | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving firstName,lastName and password values as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId                             | password | message                                            | 
      |           |          | 02451   | nameAndPasswordAsEmpty@savewave.com |          | Please include name, ZIP code, email and password. | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving email & password values as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId | password | message                                                                        | 
      | first     | last     | 02451   |         |          | Unable to create Account because Email can't be blank, Password can't be blank | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving zipCode & email values as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId | password | message                                            | 
      | first     | last     |         |         | password | Please include name, ZIP code, email and password. | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button  by giving zipCode & password values as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId                            | password | message                                            | 
      | first     | last     |         | zipAndPasswordAsEmpty@savewave.com |          | Please include name, ZIP code, email and password. | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving firstName,lastName, Zip code , Email Values  as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId | password | message                                            | 
      |           |          |         |         | password | Please include name, ZIP code, email and password. | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving firstName,lastName, Zip code, Password  values as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId               | password | message                                            | 
      |           |          |         | allEmpty@savewave.com |          | Please include name, ZIP code, email and password. | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving zip code,email & password values as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId | password | message                                            | 
      | first     | last     |         |         |          | Please include name, ZIP code, email and password. | 
  
  @Reg
  Scenario Outline: Signup using Join Now Button by giving firstname,lastname,email &password values as empty & remaining all fields with valid details 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And enter email Address as "<emailId>" 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user sees an validation message as "<message>" 
  
    Examples: 
      | firstName | lastName | zipCode | emailId | password | message                                            | 
      |           |          | 02451   |         |          | Please include name, ZIP code, email and password. | 
  
  ################################################
  
  @Reg
  Scenario Outline: Signup using single landing page of any of the coupon by giving valid credentials 
    Given a user at single landing page by using coupon title "<couponTitle>" 
      And user starts the signup process using Join Now to Get This Rebate! 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then the Rebate is Activated 
      And the account is created 
  
    Examples: 
      | couponTitle          | firstName | lastName | zipCode | password | 
      | Angel Soft® Products | test      | user     | 02451   | password | 
  
  @Reg
  Scenario Outline: Signup using any of the coupon on the home page by giving valid credentials
    Given a user is at Savingstar Home page 
     When user selects the coupon using "<couponTitle>" 
      And user starts the signup process using Join Now to Get This Rebate! 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user is on always-on page
      And the account is created 
  
    Examples: 
      | couponTitle          | firstName | lastName | zipCode | password | 
      | Angel Soft® Products | test      | user     | 02451   | password | 
  
  @Reg
  Scenario Outline: Signup using Sign In Button by giving valid credentials 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Sign In 
      And starts the signup process using Join Saving star 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp
     Then user is on always-on page 
      And the account is created 
  
    Examples: 
      | firstName | lastName | zipCode | password | 
      | test      | user     | 02451   | password | 
  
  @Reg
  Scenario Outline: Signup using join now button on snackbar by giving Valid credentials 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now on the snackbar 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user is on always-on page
      And the account is created 
  
    Examples: 
      | firstName | lastName | zipCode | password | 
      | test      | user     | 02451   | password | 
  
  @Reg
  Scenario Outline: Signup using branded experience by giving valid creds 
    Given a user at branded experience page by using "<brandedExperienceName>" 
     When user activates on the offer 
      And it is redirected to join savingstar 
      And generate unique email Id for BE Signup 
      And enter Branded Experience user email Address as generated from the above step 
      And enter Branded Experience password as "<password>" 
      And enter Branded Experience zip code as "<zipCode>" 
      And submits the BE Join request 
     Then the Branded Experience account is created 
      And the offer is in Activate state 
  
    Examples: 
      | brandedExperienceName                | password | zipCode | 
      | FRITO LAY BRANDED EXPERIENCE 8.11.16 | password | 02451   | 
  
  @Reg
  Scenario Outline: Signup using Retailer Iframe using Join SavingStar for Free by giving valid creds 
    Given a user at Iframes page by using "<iframeName>" 
     When user starts signup process using Join SavingStar for Free Today 
      And generate unique email Id for Iframes Signup 
      And enter Iframe user email Address as generated from the above step 
      And enter Iframe emailConfirm as email Address 
      And enter Iframe password as "<password>"
      And submits the Create Account request 
     Then Iframes account is created 
  
    Examples: 
      | iframeName   | password | 
      | Big Y iFrame | password | 
  
  @Reg
  Scenario Outline: Signup using Retailer Iframe using Join Now by giving valid creds 
    Given a user at Iframes page by using "<iframeName>" 
     When user starts signup process using Join SavingStar 
      And generate unique email Id for Iframes Signup
      And enter Iframe user email Address as generated from the above step 
      And enter Iframe emailConfirm as email Address 
      And enter Iframe password as "<password>"
      And submits the Create Account request 
     Then Iframes account is created 
  
    Examples: 
      | iframeName   | password | 
      | Big Y iFrame | password | 
  
  @Reg
  Scenario Outline: Signup using Retailer Iframe using Login Link by giving valid creds 
    Given a user at Iframes page by using "<iframeName>" 
     When user starts signup process using Login 
      And user starts signup using Register Today 
      And generate unique email Id for Iframes Signup
      And enter Iframe user email Address as generated from the above step 
      And enter Iframe emailConfirm as email Address 
      And enter Iframe password as "<password>"
      And submits the Create Account request 
     Then Iframes account is created 
  
    Examples: 
  
      | iframeName   | password | 
      | Big Y iFrame | password | 
  
  #####
  
  @Pending
  Scenario Outline: Signup using Join Now Button by giving Facebook credentials 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And uses Sign up with facebook to get signup pop 
      And enter facebook username as "<userName>" 
      And enter facebook password as "<password>" 
      And submits the request for SignUp 
     Then the account is created 
  
    Examples: 
      | userName         | password | 
      | testuser@xyz.com | password | 
  
  @Pending
  Scenario Outline: Signup using Sign In Button by giving facebook credentials 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Sign In 
      And starts the signup process using Join Saving star 
      And uses Sign up with facebook to get signup pop 
      And enter facebook username as "<userName>" 
      And enter facebook password as "<password>" 
      And submits the request for SignUp 
     Then the account is created 
  
    Examples: 
      | userName         | password | 
      | testuser@xyz.com | password | 
  
  @Pending
  Scenario Outline: Signup  using join now button on snackbar by giving Facebook credentials 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now on the snackbar 
      And uses Sign up with facebook to get signup pop 
      And enter facebook username as "<userName>" 
      And enter facebook password as "<password>" 
      And submits the request for SignUp 
     Then the account is created 
  
    Examples: 
      | userName | password | 
  
  @Pending
  Scenario Outline: Signup using single landing page of any of the coupon by giving facebook credentials 
    Given a user at single landing page by using coupon title "<couponTitle>" 
      And user starts the signup process using Join Now to Get This Rebate! 
      And uses Sign up with facebook to get signup pop 
      And enter facebook username as "<userName>" 
      And enter facebook password as "<password>" 
      And submits the request for SignUp 
     Then the account is created 
  
    Examples: 
      | couponTitle          | userName | password | 
      | Angel Soft® Products | userName | password | 
  
  @Pending
  Scenario Outline: Signup using any of the coupon on the home page by giving facebook credentials 
    Given a user is at Savingstar Home page 
     When user selects the coupon using "<couponTitle>" 
      And user starts the signup process using Join Now to Get This Rebate! 
      And uses Sign up with facebook to get signup pop 
      And enter facebook username as "<userName>" 
      And enter facebook password as "<password>" 
      And submits the request for SignUp 
     Then the account is created 
  
    Examples: 
      | couponTitle          | userName         | password | 
      | Angel Soft® Products | testuser@xyz.com | password | 
  
  ################################################
  
  @Reg
  Scenario Outline: Signup using any of the coupon on the home page by giving valid credentials
    Given a user is at Savingstar Home page 
     When user selects the coupon using "<couponTitle>" 
      And user starts the signup process using Join Now to Get This Rebate! 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user is on always-on page
      And the account is created 
  
    Examples: 
      | couponTitle          | firstName | lastName | zipCode | password | 
      | Angel Soft® Products | test      | user     | 02451   | password | 
  
  @Reg
  Scenario Outline: Signup using Sign In Button by giving valid credentials 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Sign In 
      And starts the signup process using Join Saving star 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp
     Then user is on always-on page 
      And the account is created 
  
    Examples: 
      | firstName | lastName | zipCode | password | 
      | test      | user     | 02451   | password | 
  
  @Reg
  Scenario Outline: Signup using join now button on snackbar by giving Valid credentials 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now on the snackbar 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
     Then user is on always-on page
      And the account is created 
  
    Examples: 
      | firstName | lastName | zipCode | password | 
      | test      | user     | 02451   | password | 
  
  @Reg
  Scenario Outline: Signup using branded experience by giving valid creds 
    Given a user at branded experience page by using "<brandedExperienceName>" 
     When user activates on the offer 
      And it is redirected to join savingstar 
      And generate unique email Id for BE Signup 
      And enter Branded Experience user email Address as generated from the above step 
      And enter Branded Experience password as "<password>" 
      And enter Branded Experience zip code as "<zipCode>" 
      And submits the BE Join request 
     Then the Branded Experience account is created 
      And the offer is in Activate state 
  
    Examples: 
      | brandedExperienceName                | password | zipCode | 
      | FRITO LAY BRANDED EXPERIENCE 8.11.16 | password | 02451   | 
  
  @Reg
  Scenario Outline: Signup using Retailer Iframe using Join SavingStar for Free by giving valid creds 
    Given a user at Iframes page by using "<iframeName>" 
     When user starts signup process using Join SavingStar for Free Today 
      And generate unique email Id for Iframes Signup 
      And enter Iframe user email Address as generated from the above step 
      And enter Iframe emailConfirm as email Address 
      And enter Iframe password as "<password>"
      And submits the Create Account request 
     Then Iframes account is created 
  
    Examples: 
      | iframeName   | password | 
      | Big Y iFrame | password | 
  
  @Reg
  Scenario Outline: Signup using Retailer Iframe using Join Now by giving valid creds 
    Given a user at Iframes page by using "<iframeName>" 
     When user starts signup process using Join SavingStar 
      And generate unique email Id for Iframes Signup
      And enter Iframe user email Address as generated from the above step 
      And enter Iframe emailConfirm as email Address 
      And enter Iframe password as "<password>"
      And submits the Create Account request 
     Then Iframes account is created 
  
    Examples: 
      | iframeName   | password | 
      | Big Y iFrame | password | 
  
  @Reg
  Scenario Outline: Signup using Retailer Iframe using Login Link by giving valid creds 
    Given a user at Iframes page by using "<iframeName>" 
     When user starts signup process using Login 
      And user starts signup using Register Today 
      And generate unique email Id for Iframes Signup
      And enter Iframe user email Address as generated from the above step 
      And enter Iframe emailConfirm as email Address 
      And enter Iframe password as "<password>"
      And submits the Create Account request 
     Then Iframes account is created 
  
    Examples: 
  
      | iframeName   | password | 
      | Big Y iFrame | password | 
  
  #####
  
  @Pending
  Scenario Outline: Signup using Join Now Button by giving Facebook credentials 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And uses Sign up with facebook to get signup pop 
      And enter facebook username as "<userName>" 
      And enter facebook password as "<password>" 
      And submits the request for SignUp 
     Then the account is created 
  
    Examples: 
      | userName         | password | 
      | testuser@xyz.com | password | 
  
  @Pending
  Scenario Outline: Signup using Sign In Button by giving facebook credentials 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Sign In 
      And starts the signup process using Join Saving star 
      And uses Sign up with facebook to get signup pop 
      And enter facebook username as "<userName>" 
      And enter facebook password as "<password>" 
      And submits the request for SignUp 
     Then the account is created 
  
    Examples: 
      | userName         | password | 
      | testuser@xyz.com | password | 
  
  @Pending
  Scenario Outline: Signup  using join now button on snackbar by giving Facebook credentials 
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now on the snackbar 
      And uses Sign up with facebook to get signup pop 
      And enter facebook username as "<userName>" 
      And enter facebook password as "<password>" 
      And submits the request for SignUp 
     Then the account is created 
  
    Examples: 
      | userName | password | 
  
  @Pending
  Scenario Outline: Signup using single landing page of any of the coupon by giving facebook credentials 
    Given a user at single landing page by using coupon title "<couponTitle>" 
      And user starts the signup process using Join Now to Get This Rebate! 
      And uses Sign up with facebook to get signup pop 
      And enter facebook username as "<userName>" 
      And enter facebook password as "<password>" 
      And submits the request for SignUp 
     Then the account is created 
  
    Examples: 
      | couponTitle          | userName | password | 
      | Angel Soft® Products | userName | password | 
  
  @Pending
  Scenario Outline: Signup using any of the coupon on the home page by giving facebook credentials 
    Given a user is at Savingstar Home page 
     When user selects the coupon using "<couponTitle>" 
      And user starts the signup process using Join Now to Get This Rebate! 
      And uses Sign up with facebook to get signup pop 
      And enter facebook username as "<userName>" 
      And enter facebook password as "<password>" 
      And submits the request for SignUp 
     Then the account is created 
  
    Examples: 
      | couponTitle          | userName         | password | 
      | Angel Soft® Products | testuser@xyz.com | password | 
  
  
