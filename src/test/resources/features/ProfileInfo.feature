Feature: profile updation feature
Descripton: profile updation Scenarios 

@Reg
  Scenario Outline: Sign in and update the personal information
    Given a user is at savingstar home page
    When user starts signin process by using the Sign In button
    And enter email Address as "<email>" on Sign Page
    And enter password as "<password>" on Sign Page
    And submits the request for Sign In
    And Click on more drop down
    And Click on your info button
    Then Update the lastname as "<lastname>" on profile page
    And Update the address1 with "<addressLineOne>" on profile page
    And Update the address2 with "<addressLineTwo>" on profile page
    And Update the gender on profile page
    And Update the city with "<city>" on profile page
    And Click on chnage personal information button
    And Verify the success message
  
    Examples: 
      | email          | password | lastname | addressLineOne | addressLineTwo | city      |
      | test@gmail.com | test@123 | lastname | address1       | Address2       | Hyderabad |
      
 
@Reg
Scenario Outline: Sign in and update the personal information with invalid zipcode
    Given a user is at savingstar home page
    When user starts signin process by using the Sign In button
    And enter email Address as "<email>" on Sign Page
    And enter password as "<password>" on Sign Page
    And submits the request for Sign In
    And Click on more drop down
    And Click on your info button
    And Update the zipcode with "<zipcode>"
    And Click on chnage personal information button
    Then Verify the message
    
    Examples: 
      | email          | password | zipcode | 
      | test@gmail.com | test@123 | asdfg   |
      
@Reg
Scenario Outline: Signup as a new user and verify the details in profile page
	Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now 
    And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    And Click on may be later link
    And Click on more drop down
    And Click on your info button
    Then Verify the first name with "<firstName>"
    And Verify the lastName with "<lastName>"
    And verify the zipcode with "<zipCode>"
    
    Examples: 
      | firstName | lastName | zipCode | password |
      | Admin     | user     | 02451   | password |
      
@Reg
Scenario Outline: Signup as a new user and update the personal information
	Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now 
    And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    And Click on may be later link
    And Click on more drop down
    And Click on your info button
    Then Update the firstname as "<fName>" on profile page
    And Update the lastname as "<lname>" on profile page
    And Update the address1 with "<addressLineOne>" on profile page
    And Update the address2 with "<addressLineTwo>" on profile page
    And Update the gender on profile page
    And Update the city with "<city>" on profile page
    And Update the zipcode with "<zcode>"
    And Click on chnage personal information button
    And Verify the success message
    
    Examples: 
      | firstName | lastName | zipCode | password | fName     | lname    | addressLineOne | addressLineTwo | city      | zcode |
      | Admin     | user     | 02451   | password | firstName | lastname | address1       | Address2       | Hyderabad | 12569 |
      
      
@Reg
Scenario Outline: Sign up as a new user and update the personal information with invalid zipcode
	Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now 
    And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    And Click on may be later link
    And Click on more drop down
    And Click on your info button
    And Update the zipcode with "<zcode>"
    And Click on chnage personal information button
    Then Verify the message
    
     Examples: 
      | firstName | lastName | zipCode | password | zcode |
      | Admin     | user     | 02451   | password | asdf  |
     
@Reg
Scenario Outline: Sign up as a new user and update the password then signout and sign in with new password
	Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now 
    And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    And Click on may be later link
    And Click on more drop down
    And Click on your info button
    And Update the password with "<pswd>"
    And Click on chnage password button
    And Verify the updated password success message
    And Navigate to coupons page
    And Click on signout button
    And user starts signin process by using the Sign In button
    Then enter email address as generated from the above step 
    And enter password as "<pswd>" on Sign Page
    And submits the request for Sign In 
    And the account is created
    
    Examples: 
      | firstName | lastName | zipCode | password | pswd 	    |
      | Admin     | user     | 02451   | password |	newpassword |
      
      
@Reg
Scenario Outline: Sign up as a new user and update the password with lessthan 8 characters
	Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now 
    And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    And Click on may be later link
    And Click on more drop down
    And Click on your info button
    And Update the password with "<pswd>"
    And Click on chnage password button
    Then Verify the error message
    
    Examples: 
      | firstName | lastName | zipCode | password | pswd 	    |
      | Admin     | user     | 02451   | password |	word        |
     
     
@Reg
Scenario Outline: Update the birth date with below 18 years and verify
	Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now 
    And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    And Click on may be later link
    And Click on more drop down
    And Click on your info button
    And Update the birth date with below 18 years
    And Click on chnage personal information button
    Then Verify the warning message 
    
    Examples: 
      | firstName | lastName | zipCode | password | 
      | Admin     | user     | 02451   | password |	    
      
@Reg
Scenario Outline: Signup as a new user and click on the change password button with out entering in the new password textbox
	Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now 
    And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    And Click on may be later link
    And Click on more drop down
    And Click on your info button
  	And Click on chnage password button
    Then Verify the password error message 
    
    Examples: 
      | firstName | lastName | zipCode | password | 
      | Admin     | user     | 02451   | password |	    
            
@Reg
Scenario Outline: Signup as a new user and click on the details button of always on then verify the personal details with the values given at registration
	Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now 
    And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    And Click on may be later link
    And click on the details button
    Then Verify firstname with "<firstName>"
    And Verify the lastname with "<lastName>" 
    And verify the zipcode with "<zipCode>"
    
    Examples: 
      | firstName  | lastName | zipCode | password |  
      | Saving     | star     | 02451   | password | 
      
      
@Reg
Scenario Outline: Signup as a new user and updatae the profile then navigate to coupons page then click on details button of always on then verify the personal details
	Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now 
    And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    And Click on may be later link
    And Click on more drop down
    And Click on your info button
    And Update the firstname as "<fName>" on profile page
    And Update the lastname as "<lname>" on profile page
    And Update the address1 with "<addressLineOne>" on profile page
    And Update the address2 with "<addressLineTwo>" on profile page
    And Update the gender on profile page
    And Update the city with "<city>" on profile page
    And Update the zipcode with "<zcode>"
    And Click on chnage personal information button
    And Verify the success message
    And Navigate to coupons page
    Then click on the details button
    And Verify firstname with "<fName>"
    And Verify the lastname with "<lname>" 
    And verify the zipcode with "<zcode>"
    
    Examples: 
      | firstName | lastName | zipCode | password | fName    | lname    | addressLineOne | addressLineTwo | city      | zcode |
      | Admin     | user     | 02451   | password | newFName | lastname | address1       | Address2       | Hyderabad | 12569 |      