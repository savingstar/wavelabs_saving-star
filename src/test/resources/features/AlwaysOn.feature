Feature: Always On Feature covering all Scenarios
Descripton: Always On Feature all Scenarios  

@Reg
  Scenario Outline: Signup as a new user and click on enroll button in signup page then click on Enroll and continue button with out filing the details and verify the warning message
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And click on the enroll button
      Then Click on enroll and continue
      And Verify error message 
      
       Examples: 
      | firstName | lastName | zipCode | password | 
      | test      | user     | 02451   | password | 
      
      
@Reg
  Scenario Outline: Signup as a new user and click on enroll button in signup page then enter street address and click on Enroll and continue button and verify the warning message
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And click on the enroll button
      And Enter the street address as "<street>"
      Then Click on enroll and continue
      And Verify error message 
      
       Examples: 
      | firstName | lastName | zipCode | password | 
      | test      | user     | 02451   | password | 
      
@Reg
  Scenario Outline: Signup as a new user and click on enroll button in signup page then select gender and click on Enroll and continue button and verify the warning message
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And click on the enroll button
      And Select gender
      Then Click on enroll and continue
      And Verify error message 
      
       Examples: 
      | firstName | lastName | zipCode | password | 
      | test      | user     | 02451   | password | 
      
@Reg
  Scenario Outline: Signup as a new user and click on enroll button in signup page then select month and click on Enroll and continue button and verify the warning message
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And click on the enroll button
      And Click on month button and select month
      Then Click on enroll and continue
      And Verify error message 
      
       Examples: 
      | firstName | lastName | zipCode | password | 
      | test      | user     | 02451   | password | 
      
@Reg
  Scenario Outline: Signup as a new user and click on enroll button in signup page then select day and click on Enroll and continue button and verify the warning message
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And click on the enroll button
      And Click on day button and select day
      Then Click on enroll and continue
      And Verify error message 
      
       Examples: 
      | firstName | lastName | zipCode | password | 
      | test      | user     | 02451   | password | 
      
@Reg
  Scenario Outline: Signup as a new user and click on enroll button in signup page then select year and click on Enroll and continue button and verify the warning message
    Given a user is at Savingstar Home page 
     When user starts the signup process using Join Now 
      And enter firstName as "<firstName>" 
      And enter lastName as "<lastName>" 
      And enter zip code as "<zipCode>" 
      And generate unique email Id 
      And enter email address as generated from the above step 
      And enter password as "<password>" 
      And submits the request for SignUp 
      And click on the enroll button
      And Click on year button and select year
      Then Click on enroll and continue
      And Verify error message 
      
       Examples: 
      | firstName | lastName | zipCode | password | 
      | test      | user     | 02451   | password | 
      

      
@Reg
  Scenario Outline: Sign up as a new user And activate the always on before going to coupons page
   Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now
	And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    Then click on the enroll button
    And Enter the street address as "<street>"
    And Click on month button and select month
    And Click on day button and select day
    And Click on year button and select year
    And Select gender
    And Click on enroll and continue
    And Check the status of the always on enrollment  
    
    Examples: 
      | firstName  | lastName | zipCode | password | street | 
      | Saving     | star     | 02451   | password | street |

@Reg
  Scenario Outline: Signup as a new user and Enroll the always on then verify the enrolled button
	Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now 
    And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    And Click on may be later link
    Then click on the details button
   	And Enter the street address as "<street>"
    And Click on month button and select month
    And Click on day button and select day
    And Click on year button and select year
    And Select gender on always on
    And Click on enroll button
    And Check the status of the always on enrollment 
    
    Examples: 
      | firstName  | lastName | zipCode | password | street | 
      | Saving     | star     | 02451   | password | street |
      
@Reg
  Scenario Outline: Sign up as a new user and activate the always on in signup page then navigate to coupons page then quit the always on program
	Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now 
    And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    And click on the enroll button
   	And Enter the street address as "<street>"
    And Click on month button and select month
    And Click on day button and select day
    And Click on year button and select year
    And Select gender
    And Click on enroll and continue
    And Check the status of the always on enrollment
    And click on enrolled button
    Then click on quit button
    And Check the status of enrolled button
    
    Examples: 
      | firstName  | lastName | zipCode | password | street | 
      | Saving     | star     | 02451   | password | street |
      
@Reg
  Scenario Outline: Sign up as a new user then activate and de-activate the always on program in coupons page
	Given a user is at Savingstar Home page 
    When user starts the signup process using Join Now 
    And enter firstName as "<firstName>" 
    And enter lastName as "<lastName>" 
    And enter zip code as "<zipCode>" 
    And generate unique email Id 
    And enter email address as generated from the above step 
    And enter password as "<password>" 
    And submits the request for SignUp 
    And Click on may be later link
    And click on the details button
   	And Enter the street address as "<street>"
    And Click on month button and select month
    And Click on day button and select day
    And Click on year button and select year
    And Select gender on always on
    And Click on enroll button
    And Check the status of the always on enrollment
    And click on enrolled button
    Then click on quit button
    And Check the status of enrolled button
    
    Examples: 
      | firstName  | lastName | zipCode | password | street | 
      | Saving     | star     | 02451   | password | street |