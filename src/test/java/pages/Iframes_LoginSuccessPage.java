package pages;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class Iframes_LoginSuccessPage extends PageObject {

	@FindBy(xpath = "//span[contains(text(),'Your registration was successful, welcome to Savin')]")
	private WebElement loginSuccessMessage;

	@FindBy(id = "nav_logout")
	private WebElement logoutLink;

	//----operation on elements starts here------
	
	public boolean isLoginSuccessMessagePresent() {
		return element(loginSuccessMessage).isPresent();
	}

	public String getLoginSuccessMessage() {
		return element(loginSuccessMessage).getText();
	}

	public void clickLogout() {
		element(logoutLink).click();
	}
	
	public boolean isLogoutLinkVisible() {
		return element(logoutLink).isPresent();
	}

}