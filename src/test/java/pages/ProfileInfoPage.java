package pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class ProfileInfoPage extends PageObject {

	MoreButtonDropDownPage moredropdown;

	@FindBy(id = "home_logo")
	private WebElement home_logo;

	@FindBy(id = "user_personally_identifiable_information_plain_text_city")
	private WebElement update_City;

	@FindBy(id = "user_personally_identifiable_information_plain_text_address2")
	private WebElement address2;

	@FindBy(id = "user_personally_identifiable_information_plain_text_address1")
	private WebElement address1;

	@FindBy(id = "user_personally_identifiable_information_plain_text_gender_male")
	private WebElement gender_Male;

	@FindBy(id = "user_personally_identifiable_information_plain_text_gender_female")
	private WebElement gender_Female;

	@FindBy(id = "user_personally_identifiable_information_plain_text_gender_other")
	private WebElement gender_Other;

	@FindBy(id = "user_password")
	private WebElement password;

	@FindBy(id = "user_personally_identifiable_information_plain_text_zip")
	private WebElement zipcode;

	@FindBy(id = "user_personally_identifiable_information_plain_text_last_name")
	private WebElement last_Name;

	@FindBy(id = "user_personally_identifiable_information_plain_text_first_name")
	private WebElement first_name;

	@FindBy(xpath = "//input[@placeholder='First Name']")
	private WebElement alwaysOn_firstName;

	@FindBy(xpath = "//input[@placeholder='Last Name']")
	private WebElement alwaysOn_lastName;

	@FindBy(id = "update_personal_information")
	private WebElement update_Personal_Info_Button;

	@FindBy(id = "update_user_password")
	private WebElement change_Password;

	@FindBy(xpath = "//div[@class='alert alert-info']/span")
	private WebElement success_Message;

	@FindBy(xpath = "//div[@class='alert alert-error']/span")
	private WebElement error_Message;

	@FindBy(id = "user_personally_identifiable_information_birthdate")
	private WebElement date_Of_Birth;

	@FindBy(xpath = "//div[@class='ineligible_users index']//h1")
	private WebElement anger_Message;

	@FindBy(xpath = "//input[@placeholder= 'Street Address']")
	private WebElement street_Address;

	@FindBy(xpath = "//button[@class='relative buttonPrimaryClass bg-animate']")
	private WebElement enrolling_Buton;

	@FindBy(id = "month")
	private WebElement month_button;

	@FindBy(id = "day")
	private WebElement day_button;

	@FindBy(id = "year")
	private WebElement year_button;

	@FindBy(xpath = "//select[@id='month']/option[@value='02']")
	private WebElement select_Month;

	@FindBy(xpath = "//select[@id='day']/option[@value='02']")
	private WebElement select_day;

	@FindBy(xpath = "//select[@id='year']/option[@value='1998']")
	private WebElement select_year;

	@FindBy(xpath = "//select[@name='Conagra Brand-gender']/option[@value='male']")
	private WebElement select_gender;

	@FindBy(xpath = "//select[@name='ConAgra® Products-gender']/option[@value='male']")
	private WebElement select_gender_AlwaysOn;

	@FindBy(xpath = "//*[contains(text(),'Enrolled')]")
	private WebElement enrolled;

	@FindBy(xpath = "//div[@class='flex-none-l']")
	private WebElement details_Button;

	@FindBy(xpath = "//div[@title='Show Always On Programs']")
	private WebElement expand_AlwaysOn;

	@FindBy(name = "Conagra Brand-gender")
	private WebElement gender_AlwaysOn;

	// ----operation on elements starts here------

	public void update_FirstName(String firstname) {
		element(first_name).type(firstname);
	}

	public void update_lastName(String lastname) {
		element(last_Name).type(lastname);
	}

	public void update_AddressLineOne(String addressLineOne) {
		element(address1).type(addressLineOne);
	}

	public void update_AddressLineTwo(String addressLineTwo) {
		element(address2).type(addressLineTwo);
	}

	public void update_Gender() {
		gender_Male.click();
	}

	public void update_City(String city) {
		element(update_City).type(city);
	}

	public void click_On_change_Personal_Info() {
		update_Personal_Info_Button.click();
	}

	public String get_success_Message() {
		return success_Message.getText();
	}

	public String get_Error_Message() {
		return error_Message.getText();
	}

	public void update_Zipcode(String zipCode) {
		element(zipcode).type(zipCode);
	}

	public String get_FirtName() {
		return first_name.getText();
	}

	public String get_LastName() {
		return last_Name.getText();
	}

	public String get_Zipcode() {
		return zipcode.getText();
	}

	public void update_Password(String pswd) {
		element(password).type(pswd);
	}

	public void clickOn_ChangePassword() {
		change_Password.click();
	}

	public void navigate_To_CouponsPage() {
		home_logo.click();
	}

	public String verify_Update_Password_Success_Message() {
		return success_Message.getText();
	}

	public String get_Password_Error_Message() {
		return error_Message.getText();
	}

	public void update_BirthDate() {
		String date = getCurrentMonthFirstDate();
		date_Of_Birth.click();
		element(date_Of_Birth).type(date);
	}

	public static String getCurrentMonthFirstDate() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
		return df.format(c.getTime());
	}

	public String get_date_Of_Birth_Warning_Message() {
		return anger_Message.getText();
	}

	public String verify_The_NewPassword_Field() {
		return error_Message.getText();
	}

	public void enter_StreetAddress(String address) {
		element(street_Address).type(address);
	}

	public void clickON_Enroll_Button() {
		enrolling_Buton.click();
	}

	public void select_Month() {
		month_button.click();
		select_Month.click();
	}

	public void select_Year() {
		year_button.click();
		select_year.click();
	}

	public void select_Day() {
		day_button.click();
		select_day.click();
	}

	public void select_Gender() {
		Select gender = new Select(gender_AlwaysOn);
		gender.selectByVisibleText("Male");
	}

	public void select_gender_AlwaysOn() {
		select_gender_AlwaysOn.click();
	}

	public String enrolled_Status() {
		boolean isPresent = moredropdown.isHideMessagePresent();
		if (isPresent) {
			moredropdown.clickCloseHideMessage();
		}
		boolean bool = element(enrolled).isVisible();
		if (!bool) {
			expand_AlwaysOn.click();
		}
		return enrolled.getText();
	}

	public void clickOn_Details_Button() {
		details_Button.click();
	}

	public String get_FirstName_From_AlwaysOn() {
		return alwaysOn_firstName.getText();
	}

	public String get_LastName_From_AlwaysOn() {
		return alwaysOn_lastName.getText();
	}
}