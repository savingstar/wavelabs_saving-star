package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.NamedUrl;
import net.thucydides.core.annotations.NamedUrls;

@DefaultUrl("https://felb1.qa1.savingstar.com")
@NamedUrls({ @NamedUrl(name = "openIframes", url = "https://felb1.qa1.savingstar.com/iframes/{1}/coupons/") })

public class Iframes_RegistrationPage extends PageObject {

	@FindBy(id = "user_email")
	private WebElement txtBox_email;

	@FindBy(id = "user_email_confirmation")
	private WebElement txtBox_email_confirmation;

	@FindBy(id = "user_password")
	private WebElement txnBox_password;

	@FindBy(id = "user_submit")
	private WebElement btn_Create_Account;

	// ----operation on elements starts here------

	public void inputEmail(String email) {
		element(txtBox_email).clear();
		element(txtBox_email).sendKeys(email);
	}

	public void inputEmailConfirm(String emailConfirm) {
		element(txtBox_email_confirmation).clear();
		element(txtBox_email_confirmation).sendKeys(emailConfirm);
	}

	public void inputPassword(String password) {
		element(txnBox_password).clear();
		element(txnBox_password).sendKeys(password);
	}

	public void clickCreateAccount() {
		element(btn_Create_Account).click();
	}
}