package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class AlwaysOnCouponsPage extends PageObject {

	MoreButtonDropDownPage moredropdown;

	@FindBy(xpath = "//button[@class='buttonBaseClass clickable']")
	private WebElement enrolled_Quit;

	@FindBy(xpath = "//span[contains(text(),'Program Now')]")
	private WebElement quit_Conformation_Button;

	@FindBy(xpath = "//*[contains(text(),'Enrolled')]")
	private WebElement enrolled;

	@FindBy(xpath = "//div[@title='Show Always On Programs']")
	private WebElement expand_AlwaysOn;

	@FindBy(xpath = "//div[@class='flex-none-l']")
	private WebElement details_Button;

	// ----operation on elements starts here------

	public void clickOn_Quit_Button() {
		enrolled_Quit.click();
	}

	public void ClickOn_Quit_Conformation() {
		quit_Conformation_Button.click();
	}

	public void clickOn_Enrolled_Button() {
		boolean isPresent = moredropdown.isHideMessagePresent();
		if (isPresent) {
			moredropdown.clickCloseHideMessage();
		}
		boolean bool = element(enrolled).isVisible();
		if (!bool) {
			expand_AlwaysOn.click();
		}
		enrolled.click();
	}

	public String getText_DetailsButton() {
		boolean isPresent = moredropdown.isHideMessagePresent();
		if (isPresent) {
			moredropdown.clickCloseHideMessage();
		}
		return details_Button.getText();
	}
}