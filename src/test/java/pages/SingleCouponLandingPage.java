package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.NamedUrl;
import net.thucydides.core.annotations.NamedUrls;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
@NamedUrls({ @NamedUrl(name = "openSpecificOffer", url = "http://www10.qa1.savewave.com/coupons/{1}/") })
public class SingleCouponLandingPage extends PageObject {

	@FindBy(xpath = "//button[normalize-space()='Join Now to Get This Rebate!']")
	private WebElement btn_joinNow;

	@FindBy(xpath = "//span[normalize-space()='Sign In']")
	private WebElement btn_SignIn;

	@FindBy(xpath = "//div[@class='activateButton']")
	private WebElement individualcouponpage_activatebtn;

	@FindBy(xpath = "//button[contains(text(),'Rebate Activated')]")
	private WebElement rebateActivatedText;

	@FindBy(xpath = "//a[contains(text(),'See All SavingStar Rebates')]")
	private WebElement seeAllSSRebatesLink;

	public String verifyIndividualCouponPage() {
		String stringval = element(individualcouponpage_activatebtn).getText();
		return stringval;
	}

	public boolean isRebatedActivated() {
		return element(rebateActivatedText).isPresent();
	}

	public void clickSeeAllRebates() {
		element(seeAllSSRebatesLink).click();
	}

	public void clickJoinNow() {
		element(btn_joinNow).click();
	}

	public void clickSignIn() {
		element(btn_SignIn).click();
	}
}

// SingleCouponLandingPage
// CouponPage