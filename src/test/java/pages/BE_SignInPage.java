package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class BE_SignInPage extends PageObject {
	
	@FindBy(xpath = "//input[@id='user_email']")
	private WebElement txt_BEemailaddress;

	@FindBy(xpath = "//input[@id='user_password']")
	private WebElement txt_BEpassword;

	@FindBy(xpath = "//input[@name='commit']")
	private WebElement btn_submit;

	public void enterBEemailaddress(String emailaddress) {
		element(txt_BEemailaddress).type(emailaddress);
	}

	public void enterBEpassword(String password) {
		element(txt_BEpassword).type(password);
	}

	public void clickSubmit() {
		element(btn_submit).click();
	}

}
