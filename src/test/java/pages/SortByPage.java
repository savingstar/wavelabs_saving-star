package pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class SortByPage extends PageObject {

	@FindBy(xpath = "//span[contains(text(),'Sort')]")
	private WebElement sortBy;

	@FindBy(xpath = "//span[contains(text(),'Reward Amount')]")
	private WebElement sortBy_reward_Amount;

	@FindBy(xpath = "//span[contains(text(),'Product Name')]")
	private WebElement sortBy_ProductName;

	@FindBy(xpath = "//span[starts-with(@class, 'relative dark-slate')]")
	private List<WebElement> reward_Amount;

	@FindBy(xpath = "//span[@class='fw5 o-70 dark-slate']")
	private List<WebElement> productName;

	public void clickOn_SortBy() {
		sortBy.click();
	}

	public void Select_SortBy_RewardAmount() {
		sortBy_reward_Amount.click();
	}

	public void Select_SortBy_ProductName() {
		sortBy_ProductName.click();
	}

	public List<WebElement> get_RewardAmount() {
		return reward_Amount;
	}

	public List<WebElement> get_ProductNmaes() {
		return productName;
	}

}
