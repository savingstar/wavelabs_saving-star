package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class BE_HomePage extends PageObject {

	@FindBy(xpath = "//span[normalize-space()='My Account']")
	private WebElement lnkTxt_MyAccount;

	@FindBy(xpath = "//a[normalize-space()='Log In']")
	private WebElement btn_LogIn;

	@FindBy(xpath = "//a[normalize-space()='Activate Your Offer']")
	private WebElement btn_Activate_Your_Offer;

	//----operation on elements starts here------
	
	public void clickLogin() {
		element(btn_LogIn).click();
	}

	public void clickActivateYourOffer() {
		element(btn_Activate_Your_Offer).click();
	}

}