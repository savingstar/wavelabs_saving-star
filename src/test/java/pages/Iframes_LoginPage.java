package pages;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class Iframes_LoginPage extends PageObject {

	@FindBy(id = "login_user_email")
	private WebElement txtbx_email;

	@FindBy(id = "login_user_password")
	private WebElement txtbx_password;

	@FindBy(id = "login_user_submit")
	private WebElement login_user_submit;

	@FindBy(xpath = "//a[contains(text(),'Register Today')]")
	private WebElement lnkTxt_Register_Today;

	//----operation on elements starts here------
	
	public void inputEmail(String email) {
		element(txtbx_email).clear();
		element(txtbx_email).sendKeys(email);
	}

	public void inputPassword(String password) {
		element(txtbx_password).clear();
		element(txtbx_password).sendKeys(password);
	}

	public void clickLogin() {
		element(login_user_submit).click();
	}

	public void clickRegisterToday() {
		element(lnkTxt_Register_Today).click();
	}

}
