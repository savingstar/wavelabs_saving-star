package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.NamedUrl;
import net.thucydides.core.annotations.NamedUrls;

//@DefaultUrl("https://felb1.qa1.savingstar.com/branded_experience/5b88a70fdbac350800000001/landing_page?by_id_only=true")
@DefaultUrl("https://felb1.qa1.savingstar.com")
@NamedUrls({
		@NamedUrl(name = "openBE", url = "https://felb1.qa1.savingstar.com/branded_experience/{1}/landing_page/") })

public class BE_LandingPage extends PageObject {

	@FindBy(xpath = "//span[normalize-space()='My Account']")
	private WebElement lnkTxt_MyAccount;

	@FindBy(xpath = "//a[normalize-space()='Log In']")
	private WebElement btn_LogIn;

	// div[@class='activate_this_rebate desktop']/div/a[text()='Activate YourOffer']
	@FindBy(xpath = "//a[normalize-space()='Activate Your Offer']")
	private WebElement btn_Activate_Your_Offer;

	@FindBy(xpath = "//a[@class='main_color']")
	private WebElement btn_navtoSingInForm;

	public void clickLogin() {
		element(btn_LogIn).click();
	}

	public void navigateToBESignInForm() {
		element(btn_navtoSingInForm).click();
	}

	public void clickActivateYourOffer() {
		// this.getJavascriptExecutorFacade().executeScript("arguments[0].scrollIntoView();",
		// btn_Activate_Your_Offer);
		element(btn_Activate_Your_Offer).click();
	}

	/*
	 * public String getBEURL(String offertitle, String stackValue) { List<Document>
	 * listDocument = beColl.getBrandedExperienceIdUsingName(offertitle);
	 * System.out.println(listDocument.size()); Document doc = listDocument.get(0);
	 * String mongoId = doc.getObjectId("_id").toString(); String beUrl =
	 * GlobalConstants.formBrandedExperienceUrl(stackValue, mongoId); return beUrl;
	 * }
	 */

}