package pages;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class AlwaysOnSignUpPage extends PageObject {

	@FindBy(xpath = "//h1[contains(text(),'Thanks for joining')]")
	private WebElement joining_txt;

	@FindBy(xpath = "//a[contains(text(),'Maybe later')]")
	private WebElement maybeLaterLink;

	@FindBy(id = "finish-button")
	private WebElement finish_button;

	@FindBy(xpath = "//button[@class= 'aoBaseButtonClass mb3 aoButtonClass']")
	private WebElement enroll_Button;

	@FindBy(xpath = "//p[@class='error']")
	private WebElement enroll_Error_Message;

	//----operation on elements starts here------

	public void clickMayBeLaterLink() {
		this.maybeLaterLink.click();
	}

	public boolean joiningTxtIsVisible() {
		return element(joining_txt).isCurrentlyVisible();
	}

	public String getJoiningText() {
		return element(joining_txt).getText();
	}

	public void clickON_Enroll_And_Continue() {
		finish_button.click();
	}

	public void clickOn_Enroll_Button(){
		enroll_Button.click();
	}

	public String get_enroll_Error_Message(){
		return enroll_Error_Message.getText();
	}
}