package pages;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

import pages.Base;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class BE_LoginSuccessPage extends PageObject {

	Base base;
	
	@FindBy(xpath = "//span[contains(text(),'My Account')]")
	private WebElement myAccountTab;

	@FindBy(xpath = "//div[contains(@class,'rebate_activated')]/span")
	private WebElement gettextofbutton;

	@FindBy(xpath = "//span[contains(text(),'My Account')]")
	private WebElement myAccountLink;

	@FindBy(id = "nav_logout")
	private WebElement logoutLink;

	@FindBy(xpath = "//div[@class='brand_message center main_color']")
	private WebElement offerActivatedTxt;

	@FindBy(xpath = "//span[contains(text(),'Offer Activated')]")
	private WebElement offerActivatedButtonTxt;

	@FindBy(xpath = "//button[contains(text(),'Activate Your Offer')]")
	private WebElement btn_Activate_Offer;

	//----operation on elements starts here------
	
	public boolean isMyAccountLinkVisible() {
		return element(myAccountLink).isDisplayed();
	}

	public void clickMyAccountDropDown() {
		element(myAccountLink).click();
	}

	public void clickLogoutLink() {
		element(logoutLink).click();
	}

	public String getOfferActivatedTxt() {
		return element(offerActivatedTxt).getText();// You activated an offer!
	}

	public boolean isOfferActivated() {
		return element(offerActivatedButtonTxt).isDisplayed();
	}

	public String getOfferActivatedButtonTxt() {
		return element(offerActivatedButtonTxt).getText();// Offer Activated
	}

	public String clickActivateOffer() {
		base.scrolltoelement(gettextofbutton);
		return element(gettextofbutton).getText();
	}

	public boolean verifyLoggedInonBEpage() {
		base.scrolltoelement(myAccountTab);
		return element(myAccountTab).isPresent();
		
	}

}