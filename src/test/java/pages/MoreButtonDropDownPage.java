package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class MoreButtonDropDownPage extends PageObject {

	@FindBy(xpath = "//span[@class='db']")
	protected WebElement txt_userloggedIn;

	@FindBy(xpath = "//span[text()='More']")
	private WebElement btn_More;

	@FindBy(xpath = "//span[text()='Sign Out']")
	protected WebElement btn_SignOut;

	@FindBy(xpath = "//span[contains(text(),'More')]")
	private WebElement lnkTxt_More;

	@FindBy(xpath = "//button[@title='Hide message']")
	private WebElement hideMessage;

	@FindBy(xpath = "//span[contains(text(),'Activated')]")
	private WebElement activatedTab;
	
	@FindBy(xpath = "//span[contains(text(),'Your Info')]")
	WebElement your_info;

	public void clickActivatedTab() {
		element(activatedTab).click();
	}

	public void clickCloseHideMessage() {
		element(hideMessage).click();
	}

	public boolean isHideMessagePresent() {
		// return element(hideMessage).isDisplayed();
		return element(hideMessage).isPresent();
	}

	public void clickMoreLink() {
		element(lnkTxt_More).click();
	}

	public void SignOutPage() {
		this.getJavascriptExecutorFacade().executeScript("arguments[0].scrollIntoView();", btn_SignOut);
		btn_SignOut.click();
	}

	public void hoverAndClick(WebElement elementToHover, WebElement elementToClick) {
		this.withAction().moveToElement(elementToHover).click(elementToClick).build().perform();
	}

	public String verifySignInUser() {
		hoverAndClick(btn_More, btn_More);
		this.getJavascriptExecutorFacade().executeScript("arguments[0].scrollIntoView();", txt_userloggedIn);
		String useremailaddres = element(txt_userloggedIn).getText();
		return useremailaddres;
	}
	
	public void click_On_YourInfo(){
		your_info.click();
	}
}
