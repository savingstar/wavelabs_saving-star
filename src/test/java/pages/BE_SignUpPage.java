package pages;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class BE_SignUpPage extends PageObject {

	@FindBy(xpath = "//a[contains(text(), 'new to SavingStar')]")
	private WebElement linkTxt_newToSavinStar;

	@FindBy(id = "user_email")
	private WebElement txt_EmailId;

	@FindBy(id = "user_password")
	private WebElement txt_Password;

	@FindBy(id = "user_personally_identifiable_information_plain_text_zip")
	private WebElement txt_zipCode;

	@FindBy(xpath = "//input[@type='submit']")
	private WebElement btn_JoinNow;

	//----operation on elements starts here------
	
	public void inputEmailAddress(String emailId) {
		element(txt_EmailId).clear();
		element(txt_EmailId).sendKeys(emailId);
	}

	public void inputPassword(String password) {
		element(txt_Password).clear();
		element(txt_Password).sendKeys(password);
	}

	public void inputZipCode(String zipCode) {
		element(txt_zipCode).clear();
		element(txt_zipCode).sendKeys(zipCode);
	}

	public void clickJoinButton() {
		element(btn_JoinNow).click();
	}
}