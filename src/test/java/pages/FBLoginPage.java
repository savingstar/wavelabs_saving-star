package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class FBLoginPage extends PageObject {
	// fb email input box
	//@FindBy(xpath = "//input[@id='email']")
	@FindBy(id="email")
	private WebElement txt_fbemail;

	// fb password input box
	//@FindBy(xpath = "//input[@id='pass']")
	@FindBy(id="pass")
	private WebElement txt_fbpassword;

	// fb login button
	//@FindBy(xpath = "//button[@id='loginbutton']")
	@FindBy(id="loginbutton")
	private WebElement btn_fblogin;

	public void enterfbEmailAddress(String email_address) {
		element(txt_fbemail).type(email_address);
	}

	public void enterfbPassword(String password) {
		element(txt_fbpassword).type(password);
	}

	public void LoginwithFB() {
		element(btn_fblogin).click();
	}

}