package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class ForgotPasswordPage extends PageObject {

	// sign in link in Forgot password modal
	@FindBy(xpath = "//a[text()='Sign In']")
	private WebElement link_SignIn_Forforpwd_modal;

	public void clickSignIn_ForgotpwdModal() {
		element(link_SignIn_Forforpwd_modal).click();
	}
}
