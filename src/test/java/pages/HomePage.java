package pages;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

import pages.Base;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class HomePage extends PageObject {

	Base base;
	
	
	@FindBy(id = "joinNow")
	private WebElement btn_JoinNow;
	
	@FindBy(xpath = "//span[text()='Sign In']")
	private WebElement btn_SignIn;

	@FindBy(xpath = "//button[@class='join']")
	private WebElement btn_JoinNow_Snackbar;

	@FindBy(xpath = "//*[contains(@class,'offersContainer')]/div[1]")
	private WebElement firstofferlink;
	
	@FindBy(xpath = "//*[contains(@class,'offersContainer')]/div[2]/div/a[@class='offerTileLink']")
	private WebElement gethrefval;

	//----operation on elements starts here------
	
	public void clickJoinNowbtn() {
		base.scrolltoelement(btn_JoinNow);
		element(btn_JoinNow).waitUntilVisible();
		element(btn_JoinNow).click();
		element(SignUpPopup.btn_JNSignIn).click();
	}

	public void clickOnOfferLink() {
		element(firstofferlink).click();
	}

	public void clickSignInButton() {
		element(btn_SignIn).click();
	}

	public Boolean verifyHomePage() {
		Boolean verifyJoinNowbtn = element(btn_JoinNow).isVisible();
		return verifyJoinNowbtn;
	}

	public void clickJoinNowButton() {
		this.btn_JoinNow.click();
	}

	public void clickJoinNowOnSnackBar() {
		this.btn_JoinNow_Snackbar.click();
	}

	public WebElement getJoinNowWebElementReference() {
		return element(this.btn_JoinNow_Snackbar);
	}
	
	public String openindividualrebate() {
		  String hrefval=element(gethrefval).getAttribute("href");
		  return hrefval;	  
	  }

}