package pages;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class SignUpPopup extends PageObject {

	@FindBy(xpath = "//*[@id=\"app\"]/div[3]/div/div/main/div/p[2]/a")
	protected static WebElement btn_JNSignIn;

	@FindBy(name = "user[first-name]")
	private WebElement txtbx_firstName;

	@FindBy(name = "user[last-name]")
	private WebElement txtbx_lastName;

	@FindBy(name = "user[zip]")
	private WebElement txtbx_zip;

	@FindBy(name = "user[email]")
	private WebElement txtbx_email;

	@FindBy(name = "user[Password]")
	private WebElement txtbx_password;

	@FindBy(xpath = "//button[normalize-space()='Sign Up with Email']")
	private WebElement btn_SignupEmail;

	@FindBy(xpath = "//button[text()='Sign In with Facebook']")
	private WebElement btn_SignupFacebook;

	@FindBy(xpath = "//a[contains(text(), 'Sign In')]")
	private WebElement signInLink;

	@FindBy(xpath = "//p[@class='error']")
	private WebElement errorMessage;

	@FindBy(xpath = "//h1[contains(text(),'Thanks for joining')]")
	private WebElement joining_txt;

	@FindBy(xpath = "//button[normalize-space()='Join Now to Get This Rebate!']")
	private WebElement btn_JoinNow;

	@FindBy(xpath = "//button[@class='loggedoutJoin']")
	WebElement loggedoutJoin;

	//----operation on elements starts here------

	public void clickJoinNow() {
		this.btn_JoinNow.click();
	}

	public void loggedoutJoin(){ 
		loggedoutJoin.click();
	}

	public void setFirstName(String firstName) {
		element(txtbx_firstName).clear();
		element(txtbx_firstName).type(firstName);
	}

	public void setLastName(String lastName) {
		element(txtbx_lastName).clear();
		element(txtbx_lastName).type(lastName);
	}

	public void setZipCode(String zipCode) {
		element(txtbx_zip).clear();
		element(txtbx_zip).type(zipCode);
	}

	public void setEmail(String email) {
		element(txtbx_email).clear();
		element(txtbx_email).type(email);
	}

	public void setPassword(String password) {
		element(txtbx_password).clear();
		element(txtbx_password).type(password);
	}

	public void clickSignupEmailButton() {
		element(btn_SignupEmail).click();
	}

	public void clickSignupFacebbookButton() {
		element(btn_SignupFacebook).click();
	}

	public void clickSignInLink() {
		this.signInLink.click();
	}

	public String getErrorMessage() {
		return this.errorMessage.getText();
	}

	public boolean isSignedup() {
		boolean text = joining_txt.isDisplayed();
		return text;
	}
}

