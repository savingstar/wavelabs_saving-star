package pages;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class IndividualCouponPage extends PageObject {

	@FindBy(xpath = "//div[@class='activateButton']")
	private WebElement individualcouponpage_activatebtn;

	//----operation on elements starts here------
	
	public String verifyIndividualCouponPage() {
		String stringval = element(individualcouponpage_activatebtn).getText();
		return stringval;
	}
}