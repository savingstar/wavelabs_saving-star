package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class Iframes_HomePage extends PageObject {

	// a[normalize-space()='Join SavingStar']
	@FindBy(id = "signup_link")
	private WebElement lnkTxt_JoinSavingStar;

	// a[normalize-space()='Join SavingStar for Free Today!']
	@FindBy(id = "main_join_more")
	private WebElement btn_joinSavingStarForFreeToday;

	@FindBy(id = "direct_login")
	private WebElement lntTxt_Login;

	public void clickJoinSavingStarLink() {
		element(lnkTxt_JoinSavingStar).click();
	}

	public void clickLoginLink() {
		element(lntTxt_Login).click();
	}

	public void clickJoinSavingStarButton() {
		element(btn_joinSavingStarForFreeToday).click();
	}

}