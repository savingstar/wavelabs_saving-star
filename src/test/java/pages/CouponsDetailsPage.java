package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.NamedUrl;
import net.thucydides.core.annotations.NamedUrls;

@DefaultUrl("http://www10.qa1.savewave.com")
@NamedUrls({ @NamedUrl(name = "openSpecificOffer", url = "http://www10.qa1.savewave.com/coupons/{1}/") })

public class CouponsDetailsPage extends PageObject {

	// click on Sing In link in the Rebate pop up
	@FindBy(xpath = "//a[text()='sign in.']")
	private WebElement signInlink_offerPopup;

	// click on Details button of Always On rebate
	@FindBy(xpath = "//*[@id=\"app\"]/div[2]/div[2]/section/div[2]/div[1]/div[1]/div/div[2]/button[text()='Details']")
	private WebElement btn_Details_AOsection;

	@FindBy(xpath = "//button[normalize-space()='Join Now to Get This Rebate!']")
	private WebElement btn_JoinNow;

	public void clickJoinNow() {
		this.btn_JoinNow.click();
	}

	public void clickSignInlinkOfferPopup() {
		element(signInlink_offerPopup).click();
	}

	public void clickAODetailsbtn() {
		this.getJavascriptExecutorFacade().executeScript("arguments[0].scrollIntoView();", btn_Details_AOsection);
		element(btn_Details_AOsection).click();
	}

	public void clickOnCouponUsingHrefValue(String hrefValue) {
		String path = "//a[@href=\"/coupons/" + hrefValue + "\"]";
		element(By.xpath(path)).click();
	}

}

//CouponsDetailsPage
//CouponPopup
