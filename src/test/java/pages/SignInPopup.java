package pages;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

import pages.Base;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class SignInPopup extends PageObject {
	Base base;

	@FindBy(xpath = "//p[ @class='error']")
	private WebElement txt_SignInerror;

	@FindBy(xpath = "//a[contains(text(),'sign in')]")
	WebElement signIn;

	@FindBy(name="user[email]")
	private WebElement txtbx_EmailAddress;

	@FindBy(name="user[Password]")
	private WebElement txtbx_Password;

	@FindBy(xpath="//button[@type='submit']")
	private WebElement btn_SignInwithEmail;

	@FindBy(xpath="//a[contains(text(),'Forgot your password?')]")
	private WebElement link_ForgotPwd;

	@FindBy(xpath = "//button[@class='facebook']")
	private WebElement btn_SignInwithFB;

	@FindBy(xpath = "//a[contains(text(),'Join Savingstar')]")
	private WebElement lnkTxt_JoingSavingStar;


	//----operation on elements starts here------

	public void clickJoinSavingStarLink() {
		this.lnkTxt_JoingSavingStar.click();
	}

	public void enterEmailAddress(String email_address) {
		element(txtbx_EmailAddress).type(email_address);
	}

	public void enterPassword(String password) {
		element(txtbx_Password).type(password);
	}

	public void clickSignInwithEmailbtn() {
		element(btn_SignInwithEmail).click();
	}

	public String getSignInErrormessage() {
		String error_msg = txt_SignInerror.getText();
		return error_msg;
	}

	public void clickForgotPassword() {
		element(link_ForgotPwd).click();
	}

	public void clickSignInwithFB() {
		base.scrolltoelement(btn_SignInwithFB);
		element(btn_SignInwithFB).click();
	}

	public void signIn_Through_OfferLink(){
		signIn.click();
	}

}