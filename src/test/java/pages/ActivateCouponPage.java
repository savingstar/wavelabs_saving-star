package pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www10.qa1.savewave.com/coupons")
public class ActivateCouponPage extends PageObject {
	MoreButtonDropDownPage moredropdown;
	int size_AllRebates = 0;

	@FindBy(xpath = "//*[starts-with(@class,'quickActivate unactivated')]")
	private WebElement Coupon_actiavte;

	@FindBy(xpath = "//*[starts-with(@class,'quickActivate activated')]")
	private List<WebElement> coupons_activated;

	@FindBy(xpath = "//*[starts-with(@id,'want_rebate')]")
	private List<WebElement> Coupons_actiavte;

	@FindBy(xpath = "//*[starts-with(@class,'quickActivate unactivated')]/../../..//div[@class='containedImage']")
	private WebElement image;

	@FindBy(xpath = "//*[starts-with(@class,'quickActivate activated')]/../../..//div[@class='containedImage']")
	private WebElement activated_productImage;

	@FindBy(xpath = "//button[@class='relative buttonPrimaryClass']")
	private WebElement rebate_button;

	@FindBy(xpath = "//*[@class='buttonIcon inline-icon']")
	private WebElement product_detail_Popup;

	@FindBy(xpath = "//button[contains(text(),'Rebate')]")
	private WebElement activation_Text;

	@FindBy(xpath = "//span[contains(text(),'Stores')]")
	private WebElement search_button;

	@FindBy(xpath = "//span[contains(text(),'More Stores')]")
	private WebElement morestores;

	@FindBy(name = "query")
	private WebElement search_store;

	@FindBy(xpath = "//span[contains(text(),'Big Y')]")
	private WebElement select_store;

	@FindBy(xpath = "//button[@class='buttonPrimaryClass']")
	private WebElement link_button;

	@FindBy(name = "accountNumber")
	private WebElement enter_cardNumber;

	@FindBy(name = "accountNumberDupe")
	private WebElement reenter_cardNumber;

	@FindBy(name = "linkButton")
	private WebElement linkIt;

	@FindBy(xpath = "//button[@class='close']")
	private WebElement close;

	@FindBy(xpath = "//div[@class='offersContainer standard-width-plus-outer-margins']/div[1]")
	private WebElement first_Product;

	@FindBy(xpath = "//div[@class='offersContainer standard-width-plus-outer-margins']/div[2]")
	private WebElement second_Product;

	@FindBy(xpath = "//li[@title='See your activated rebates']")
	private WebElement activated_tab;

	@FindBy(xpath = "//*[starts-with(@class, 'quickActivate activated')]/../../..//h3")
	private WebElement activated_CouponText;

	// ----operation on elements starts here------

	public void activate_coupon() {
		Coupon_actiavte.click();
	}

	public String getText_Of_Coupon() {
		isHideMessagePresent();
		return activated_CouponText.getText();
	}

	public void clickON_Image() {
		image.click();
	}

	public String clickON_RebateButton() {
		rebate_button.click();
		return element(activation_Text).getText();
	}

	public void clickOn_First_Product() {
		first_Product.click();
	}

	public void clickOn_Second_Product() {
		second_Product.click();
	}

	public String Check_RebateButton() {
		return element(activation_Text).getText();
	}

	public void close_The_product_popup() {
		product_detail_Popup.click();
	}

	public void activate_coupons_for_newUser() {
		for (int i = 0; i < 3; i++) {
			isHideMessagePresent();
			Coupons_actiavte.get(i).click();
		}
	}

	public void isHideMessagePresent() {
		boolean isPresent = moredropdown.isHideMessagePresent();
		if (isPresent) {
			moredropdown.clickCloseHideMessage();
		}
	}

	public void clickOn_Activated_Coupon() {
		activated_productImage.click();
	}

	public String check_RebateButton() {
		String text = null;
		boolean notPresent = element(rebate_button).isPresent();
		if (!notPresent) {
			text = element(activation_Text).getText();
		}
		product_detail_Popup.click();
		return text;
	}

	public void count_coupons_in_ActivatedTab() {
		size_AllRebates = coupons_activated.size();
		activated_tab.click();
	}

	public boolean verify_count_of_activated_coupons() {
		int activated_coupons = coupons_activated.size();
		boolean bool = false;
		if (activated_coupons == size_AllRebates) {
			bool = true;
		}
		return bool;
	}
}