package pages;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.PageObject;

public class Base extends PageObject{

	//Hover and Click function
	public void hoverAndClick(WebElement elementToHover, WebElement elementToClick) {
		this.withAction().moveToElement(elementToHover).click(elementToClick).build().perform();
	}
	
	//scroll to element function
	public void scrolltoelement(WebElement elementToView) {
		 this.getJavascriptExecutorFacade().executeScript("arguments[0].scrollIntoView();", elementToView);	
	}
 
	 
	
}
