package steps;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.ClearCookiesPolicy;
import net.thucydides.core.annotations.Managed;
import pages.SortByPage;

public class SortByStep {

	int i = 0;

	@Managed(clearCookies = ClearCookiesPolicy.BeforeEachTest)
	WebDriver driver;

	SortByPage sortByPage;

	@And("^click on the sort by$")
	public void clickOn_SortBy() {
		sortByPage.clickOn_SortBy();
	}

	@And("^Select sort by Reward amount$")
	public void Select_SortBy_RewardAmount() {
		sortByPage.Select_SortBy_RewardAmount();
	}

	@Then("^the reward amount of the products in decending order$")
	public void verify_The_Order_Of_Reward_Amount() {
		List<WebElement> rewardAmount = sortByPage.get_RewardAmount();
		boolean bool = StringConvertionToInt(rewardAmount);
		Assert.assertEquals("Products are not sorted by reward amount", bool, true);
	}

	@And("^Select sort by product name$")
	public void Select_SortBy_ProductName() {
		sortByPage.Select_SortBy_ProductName();
	}

	@Then("^products should be sort in alphabetical order$")
	public void get_ProductNmaes() {
		List<WebElement> productNames = sortByPage.get_ProductNmaes();
		boolean bool = verify_Order_Of_Product_By_ProductNames(productNames);
		Assert.assertEquals("Products are not sorted by reward amount", bool, true);
	}

	public boolean verify_Order_Of_Product_By_ProductNames(List<WebElement> productNames) {
		boolean bool = false;
		String[] arr = new String[productNames.size()];
		String[] arr1 = new String[productNames.size()];
		for (i = 0; i < arr.length; i++) {
			arr[i] = productNames.get(i).getText();
			arr1[i] = productNames.get(i).getText();
		}
		Arrays.sort(arr);
		if (Arrays.equals(arr, arr1)) {
			bool = true;
		}
		return bool;
	}

	public boolean StringConvertionToInt(List<WebElement> rewardAmount) {
		int result = 0;
		int price[] = new int[5];
		for (i = 0; i < 5; i++) {
			String rewardPercentage = rewardAmount.get(i).getText();
			int amount = rewardPercentage.length() - 1;
			String substr = rewardPercentage.substring(0, amount);
			result = Integer.parseInt(substr);
			price[i] = result;
		}
		for (i = 0; i < price.length - 1; i++) {
			if (price[i] < price[i + 1]) {
				return false;
			}
		}
		return true;
	}
}
