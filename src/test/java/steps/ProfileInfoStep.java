package steps;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.ClearCookiesPolicy;
import net.thucydides.core.annotations.Managed;
import pages.MoreButtonDropDownPage;
import pages.ProfileInfoPage;

public class ProfileInfoStep {

	@Managed(clearCookies = ClearCookiesPolicy.BeforeEachTest)
	WebDriver driver;

	ProfileInfoPage profile;

	MoreButtonDropDownPage moredropdown;

	@Then("^Update the firstname as \"([^\"]*)\" on profile page$")
	public void update_FirstName(String firstname) {
		profile.update_FirstName(firstname);
	}

	@And("^Update the lastname as \"([^\"]*)\" on profile page$")
	public void update_lastName(String lastName) {
		profile.update_lastName(lastName);
	}

	@And("^Update the address1 with \"([^\"]*)\" on profile page$")
	public void update_AddressLineOne(String addressLineOne) {
		profile.update_AddressLineOne(addressLineOne);
	}

	@And("^Update the address2 with \"([^\"]*)\" on profile page$")
	public void update_AddressLineTwo(String addressLineTwo) {
		profile.update_AddressLineTwo(addressLineTwo);
	}

	@And("^Update the gender on profile page$")
	public void update_Gender() {
		profile.update_Gender();
	}

	@And("^Update the city with \"([^\"]*)\" on profile page$")
	public void update_City(String city) {
		profile.update_City(city);
	}

	@And("^Click on more drop down$")
	public void click_On_More() {
		moredropdown.clickMoreLink();
	}

	@And("^Click on your info button$")
	public void click_on_YourInfo() {
		moredropdown.click_On_YourInfo();
	}

	@And("Click on chnage personal information button")
	public void click_On_change_Personal_Info() {
		profile.click_On_change_Personal_Info();
	}

	@And("^Verify the success message$")
	public void get_success_Message() {
		String success_Message = profile.get_success_Message();
		Assert.assertEquals("Success message is not displayed", success_Message,
				"Your personal information has been updated");
	}

	@And("^Update the zipcode with \"([^\"]*)\"$")
	public void update_Zipcode(String zipCode) {
		profile.update_Zipcode(zipCode);
	}

	@And("Verify the message")
	public void error_Message() {
		String error_Message = profile.get_Error_Message();
		Assert.assertEquals("Failure message is not matched", error_Message,
				"There were errors in your Personal Information");
	}

	@Then("^Verify the first name with \"([^\"]*)\"$")
	public void verify_FirstName(String firstName) {
		String fName = profile.get_FirtName();
		Assert.assertEquals("First name is not matched", fName, firstName);
	}

	@And("^Verify the lastName with \"([^\"]*)\"$")
	public void verify_LastName(String lastName) {
		String lName = profile.get_LastName();
		Assert.assertEquals("Last name is not matched", lName, lastName);
	}

	@And("^verify the zipcode with \"([^\"]*)\"$")
	public void verify_Zipcode(String zipcode) {
		String zcode = profile.get_Zipcode();
		Assert.assertEquals("Zipcode is not matched", zcode, zipcode);
	}

	@And("^Update the password with \"([^\"]*)\"$")
	public void update_Password(String pswd) {
		profile.update_Password(pswd);
	}

	@And("^Click on chnage password button$")
	public void clickOn_ChangePassword() {
		profile.clickOn_ChangePassword();
	}

	@And("^Verify the updated password success message$")
	public void verify_Update_Password_Success_Message() {
		String success_message = profile.verify_Update_Password_Success_Message();
		Assert.assertEquals("Success message is not displayed", success_message, "Your password has been updated.");
	}

	@And("^Navigate to coupons page$")
	public void navigate_To_CouponsPage() {
		profile.navigate_To_CouponsPage();
	}

	@Then("^Verify the error message$")
	public void get_Password_Error_Message() {
		String error_Message = profile.get_Password_Error_Message();
		Assert.assertEquals("Error	 message is not displayed", error_Message,
				"Password must be at least 8 characters");
	}

	@And("Update the birth date with below 18 years")
	public void update_BirthDate() {
		profile.update_BirthDate();
	}

	@And("^Verify the warning message$")
	public void get_date_Of_Birth_Warning_Message() {
		String warning_message = profile.get_date_Of_Birth_Warning_Message();
		Assert.assertEquals("Warning message is not displayed", warning_message,
				"Our terms of service require that users be 18 years or older.");
	}

	@Then("^Verify the password error message$")
	public void verify_The_NewPassword_Field() {
		String message = profile.verify_The_NewPassword_Field();
		Assert.assertEquals("Error message is not displayed", message, "Password is required");
	}

	@And("^Enter the street address as \"([^\"]*)\"$")
	public void enter_StreetAddress(String address) {
		profile.enter_StreetAddress(address);
	}

	@And("^Click on enroll button$")
	public void clickON_Enroll_Button() {
		profile.clickON_Enroll_Button();
	}

	@And("^Click on month button and select month$")
	public void select_Month() {
		profile.select_Month();
	}

	@And("^Click on day button and select day$")
	public void select_Day() {
		profile.select_Day();
	}

	@And("^Click on year button and select year$")
	public void select_Year() {
		profile.select_Year();
	}

	@And("^Select gender$")
	public void select_Gender() throws Exception {
		Thread.sleep(20000);
		profile.select_Gender();
	}

	@And("^Select gender on always on$")
	public void select_gender_AlwaysOn() {
		profile.select_gender_AlwaysOn();
	}

	@And("^Check the status of the always on enrollment$")
	public void enrolled_Status() {
		String status = profile.enrolled_Status();
		Assert.assertEquals("Always on is not enrolled", status, "Enrolled");
	}

	@Then("^click on the details button$")
	public void clickOn_Details_Button() {
		profile.clickOn_Details_Button();
	}

	@And("^Verify firstname with \"([^\"]*)\"$")
	public void get_FirstName_From_AlwaysOn(String firstname) {
		String fname = profile.get_FirstName_From_AlwaysOn();
		Assert.assertEquals("First name is not matched", fname, firstname);
	}

	@And("^Verify the lastname with \"([^\"]*)\"$")
	public void get_LastName_From_AlwaysOn(String lastname) {
		String lName = profile.get_LastName_From_AlwaysOn();
		Assert.assertEquals("Last name is not matched", lName, lastname);
	}
}