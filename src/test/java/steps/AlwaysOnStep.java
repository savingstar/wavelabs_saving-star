package steps;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.ClearCookiesPolicy;
import net.thucydides.core.annotations.Managed;
import pages.AlwaysOnCouponsPage;
import pages.AlwaysOnSignUpPage;
import pages.SignInPopup;

public class AlwaysOnStep {

	@Managed(clearCookies = ClearCookiesPolicy.BeforeEachTest)
	WebDriver driver;

	SignInPopup signInPopup;

	AlwaysOnSignUpPage alwaysOnSignUpPage;

	AlwaysOnCouponsPage alwaysOnCouponPage;

	@Then("^click on the enroll button$")
	public void clickOn_Enroll_Button() {
		alwaysOnSignUpPage.clickOn_Enroll_Button();
	}

	@And("^Click on enroll and continue$")
	public void clickON_Enroll_And_Continue() throws Exception {
		alwaysOnSignUpPage.clickON_Enroll_And_Continue();
	}

	@And("^Verify error message$")
	public void get_enroll_Error_Message() {
		String enroll_Error_Message = alwaysOnSignUpPage.get_enroll_Error_Message();
		Assert.assertEquals("Enroll error message is not displayed", enroll_Error_Message,
				"Please provide all requested information to activate this offer");
	}

	@And("^click on enrolled button$")
	public void clickOn_Enrolled_Button() {
		alwaysOnCouponPage.clickOn_Enrolled_Button();
	}

	@Then("^click on quit button$")
	public void clickOn_Quit_Button() {
		alwaysOnCouponPage.clickOn_Quit_Button();
		alwaysOnCouponPage.ClickOn_Quit_Conformation();
	}

	@And("^Check the status of enrolled button$")
	public void getText_DetailsButton() throws Exception {
		String status = alwaysOnCouponPage.getText_DetailsButton();
		Assert.assertEquals("Details button is not displayed so it does not quits the always on program", status,
				"Details");
	}
}