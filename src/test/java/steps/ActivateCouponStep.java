package steps;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.ClearCookiesPolicy;
import net.thucydides.core.annotations.Managed;
import pages.AlwaysOnSignUpPage;
import pages.ActivateCouponPage;
import pages.MoreButtonDropDownPage;
import pages.SignInPopup;

public class ActivateCouponStep {
	@Managed(clearCookies = ClearCookiesPolicy.BeforeEachTest)
	WebDriver driver;

	SignInPopup signInPopup;

	ActivateCouponPage couponActivation;

	AlwaysOnSignUpPage alwaysOnSignUpPage;

	MoreButtonDropDownPage moredropdown;

	String activated_couponText;

	@Then("^Click on plus button of the product in coupons page$")
	public void user_activate_the_coupons() throws Exception {
		Thread.sleep(5000);
		couponActivation.activate_coupon();
	}

	@And("^Get the text of the activated coupon$")
	public void activateCoupon() {
		activated_couponText = couponActivation.getText_Of_Coupon();
	}

	@Then("^Verify the activated coupon in activate tab$")
	public void verify_Activated_Coupon() throws Exception {
		Thread.sleep(5000);
		String couponText_ActivatedTab = couponActivation.getText_Of_Coupon();
		Assert.assertEquals("Coupon name is not matched", couponText_ActivatedTab, activated_couponText);
	}

	@Then("^Click on the product in coupons page$")
	public void clickON_Image() {
		couponActivation.clickON_Image();
	}

	@And("^Close hide message and click on the signout button$")
	public void click_signoutButton() throws Exception {
		couponActivation.close_The_product_popup();
		clickOn_HideMessage();
		moredropdown.clickMoreLink();
		moredropdown.SignOutPage();
	}

	@And("^Click on signout button$")
	public void clickOn_signoutButton() throws Exception {
		moredropdown.clickMoreLink();
		moredropdown.SignOutPage();
	}

	@Then("^Click on activate the rebate button and verify the rebate button is activated$")
	public void clickON_RebateButton() {
		String success_text = couponActivation.clickON_RebateButton();
		Assert.assertEquals("Rebate activated message is not matched", success_text, "Rebate Activated");
	}

	@And("^Click on may be later link$")
	public void click_on_maybe_later() throws Exception {
		alwaysOnSignUpPage.clickMayBeLaterLink();
		clickOn_HideMessage();
	}

	@Then("^Coupon should be activated for the product$")
	public void check_Status_of_The_Product() {
		couponActivation.clickOn_Activated_Coupon();
		check_RebateButton();
	}

	public void clickOn_HideMessage() throws Exception {
		Thread.sleep(5000);
		boolean isPresent = moredropdown.isHideMessagePresent();
		if (isPresent) {
			moredropdown.clickCloseHideMessage();
		}
	}

	@Then("^Activate the coupons for the new user$")
	public void activate_all_coupons() throws Exception {
		couponActivation.activate_coupons_for_newUser();
	}

	@And("^Click on the product in coupons page which is already activated$")
	public void clickOn_Activated_Coupon() {
		couponActivation.clickOn_Activated_Coupon();
	}

	@Then("^Check the activate the rebate button for the product$")
	public void check_RebateButton() {
		String success_text = couponActivation.check_RebateButton();
		Assert.assertEquals("Rebate activated message is not displayed", success_text, "Rebate Activated");
	}

	@Then("^Check the activation status of the product$")
	public void check_activationStatus_of_the_product() throws Exception {
		Thread.sleep(5000);
		String success_text = couponActivation.check_RebateButton();
		if (!success_text.equalsIgnoreCase("")) {
			Assert.assertEquals("Rebate activated message is not displayed", success_text, "Rebate Activated");
		} else {
			Assert.assertEquals("Rebate activated message is not displayed", success_text, "Rebate Activated");
		}
	}

	@And("^Click on sign in button$")
	public void signIn_Through_OfferLink() {
		signInPopup.signIn_Through_OfferLink();
	}

	@And("^Click on the first product$")
	public void clickOn_First_Product() throws Exception {
		Thread.sleep(5000);
		couponActivation.clickOn_First_Product();
	}

	@And("^click on the second product$")
	public void clickOn_Second_Product() {
		couponActivation.clickOn_Second_Product();
	}

	@And("^Navigateto activate tab$")
	public void Navigate_to_activateTab() {
		couponActivation.count_coupons_in_ActivatedTab();
	}

	@And("^Verify the count of activated coupons in activation tab$")
	public void verify_the_Count_of_activatedCoupons() {
		boolean bool = couponActivation.verify_count_of_activated_coupons();
		if (bool) {
			Assert.assertTrue("The count of activated coupons in all rebates page is matched with activated tab", bool);
		} else {
			Assert.assertTrue("The count of activated coupons in all rebates page is not matched with activated tab",
					bool);
		}
	}
}