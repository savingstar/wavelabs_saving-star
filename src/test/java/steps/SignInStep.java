package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.bson.Document;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import common.MongoIframesCollection;
import common.MongoOffersCollection;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.ClearCookiesPolicy;
import net.thucydides.core.annotations.Managed;
import pages.BE_LandingPage;
import pages.BE_LoginSuccessPage;
import pages.BE_SignInPage;
import pages.CouponsDetailsPage;
import pages.FBLoginPage;
import pages.ForgotPasswordPage;
import pages.HomePage;
import pages.Iframes_HomePage;
import pages.Iframes_LoginPage;
import pages.Iframes_LoginSuccessPage;
import pages.Iframes_RegistrationPage;
import pages.MoreButtonDropDownPage;
import pages.SignInPopup;
import pages.SignUpPopup;
import pages.SingleCouponLandingPage;

public class SignInStep {
	@Managed(clearCookies = ClearCookiesPolicy.BeforeEachTest)
	WebDriver driver;

	CouponsDetailsPage couponpopup;

	MoreButtonDropDownPage moredropdown;

	SignUpPopup signUpPopup;

	CouponsDetailsPage couponPopPage;

	FBLoginPage fblogin;

	ForgotPasswordPage forgotpasswordModal;

	HomePage homepage;

	SignInPopup signInPopup;

	SingleCouponLandingPage singleCouponLandingPage;

	BE_LandingPage belandingpage;

	BE_SignInPage besigninpage;

	BE_LoginSuccessPage beloginsuccesspage;

	String useremailaddress = null;

	Iframes_HomePage iframeHomePage;

	Iframes_LoginPage iframeLoginPage;

	SignUpPopup signUpPopPage;

	Iframes_LoginSuccessPage iframesLoginSuccessPage;

	Iframes_RegistrationPage iframeRegisterPage;

	MongoOffersCollection offerscollection = new MongoOffersCollection();

	@Given("^a user is at savingstar home page$")
	public void a_user_is_at_savingstar_home_page() throws Exception {
		homepage.open();
	}

	@When("^user starts signin process by using the Join Now button$")
	public void user_starts_signin_process_by_using_the_Join_Now_button() throws Exception {
		homepage.clickJoinNowButton();
	}

	@When("^user clicks on Sign In Link$")
	public void user_clicks_on_Sign_In_Link() throws Exception {
		signUpPopPage.clickSignInLink();
	}

	@When("^enter email Address as \"([^\"]*)\" on Sign Page$")
	public void enter_email_Address_as_on_Sign_Page(String arg1) throws Exception {
		signInPopup.enterEmailAddress(arg1);
		useremailaddress = arg1;
	}

	@When("^enter password as \"([^\"]*)\" on Sign Page$")
	public void enter_password_as_on_Sign_Page(String arg1) throws Exception {
		signInPopup.enterPassword(arg1);
	}

	@When("^submits the request for Sign In$")
	public void submits_the_request_for_Sign_In() throws Exception {
		signInPopup.clickSignInwithEmailbtn();
	}

	@Then("^user sees the coupons page$")
	public void user_sees_the_coupons_page() throws Exception {
		Thread.sleep(5000);
		boolean isPresent = moredropdown.isHideMessagePresent();
		if (isPresent) {
			moredropdown.clickCloseHideMessage();
		}
		// String result = couponsPage.verifySignInUser();
		String result = moredropdown.verifySignInUser();
		Assert.assertEquals(useremailaddress, result);
		moredropdown.clickActivatedTab();
		Thread.sleep(3000);
		moredropdown.clickMoreLink();
		Thread.sleep(3000);
		moredropdown.SignOutPage();
		Thread.sleep(3000);
	}

	@When("^user starts signin process by using the Sign In button$")
	public void user_starts_signin_process_by_using_the_Sign_In_button() throws Exception {
		homepage.clickSignInButton();
	}

	@When("^user uses Sign In with Facebook button$")
	public void user_uses_Sign_In_with_Facebook_button() throws Exception {
		signInPopup.clickSignInwithFB();
	}

	@When("^submits the fb request for Sign In$")
	public void submits_the_fb_request_for_Sign_In() throws Exception {
		fblogin.LoginwithFB();
	}

	@Then("^user does not see the coupons page$")
	public void user_does_not_see_the_coupons_page() throws Exception {
		Assert.assertTrue(homepage.verifyHomePage());
	}

	@Then("^error message \"([^\"]*)\" is displayed\\.$")
	public void error_message_is_displayed(String arg1) throws Exception {
		String expected_msg = arg1;
		String actual_msg = signInPopup.getSignInErrormessage();
		Assert.assertEquals(expected_msg, actual_msg);
	}

	@When("^initiates password reset using Forgot Password link$")
	public void initiates_password_reset_using_Forgot_Password_link() throws Exception {
		signInPopup.clickForgotPassword();
	}

	@When("^user starts signin process by using the SignIn link from the Forgot Password pop up$")
	public void user_starts_signin_process_by_using_the_SignIn_link_from_the_Forgot_Password_pop_up() throws Exception {
		forgotpasswordModal.clickSignIn_ForgotpwdModal();
	}

	@When("^user tries to activate rebate using Rebate link$")
	public void user_tries_to_activate_rebate_using_Rebate_link() throws Exception {
		Thread.sleep(500);
		homepage.clickOnOfferLink();
	}

	@When("^user starts signin process by using the SignIn link from the Rebate pop up$")
	public void user_starts_signin_process_by_using_the_SignIn_link_from_the_Rebate_pop_up() throws Exception {
		couponpopup.clickSignInlinkOfferPopup();
	}

	@When("^user tries to enroll any Always On using Details button$")
	public void user_tries_to_enroll_any_Always_On_using_Details_button() throws Exception {
		couponpopup.clickAODetailsbtn();
	}

	@When("^user starts signin process by using the SignIn link from the Always On offer pop up$")
	public void user_starts_signin_process_by_using_the_SignIn_link_from_the_Always_On_offer_pop_up() throws Exception {
		couponpopup.clickSignInlinkOfferPopup();
	}

	/*@When("^user starts signin process by using the Sign In button from the individual coupon page$")
	public void user_starts_signin_process_by_using_the_Sign_In_button_from_the_individual_coupon_page()
			throws Exception {

	 * String hrefval = homepage.openindividualrebate();
	 * System.out.println(hrefval); homepage.openAt(hrefval);

		// couponpopup.clickSignInlinkOfferPopup();

	}*/

	@When("^user logins using Sign in Link$")
	public void user_logins_using_Sign_in_Link() {
		singleCouponLandingPage.clickSignIn();
	}

	@Then("^user is logged in$")
	public void user_is_logged_in() throws Exception {
		// String result = couponsPage.verifySignInUser();
		String result = moredropdown.verifySignInUser();
		Assert.assertEquals(useremailaddress, result);
	}

	@Then("^user sees the individual coupon page\\.$")
	public void user_sees_the_individual_coupon_page() throws Exception {
		String val1 = "Activate This Rebate";
		String val2 = "Rebate Activated";
		String actualres = singleCouponLandingPage.verifyIndividualCouponPage();
		Assert.assertTrue(actualres.equals(val1) || actualres.equals(val2));
	}

	@When("^user starts signin process by using the Log In button on BE page$")
	public void user_starts_signin_process_by_using_the_Log_In_button_on_BE_page() throws Exception {
		belandingpage.clickActivateYourOffer();
		belandingpage.navigateToBESignInForm();
	}

	@When("^enter email Address as \"([^\"]*)\" on BE page$")
	public void enter_email_Address_as_on_BE_page(String emailaddress) throws Exception {
		besigninpage.enterBEemailaddress(emailaddress);
	}

	@When("^enter password as \"([^\"]*)\" on BE page$")
	public void enter_password_as_on_BE_page(String password) throws Exception {
		besigninpage.enterBEpassword(password);
	}

	@When("^submits the request on BE page$")
	public void submits_the_request_on_BE_page() throws Exception {
		besigninpage.clickSubmit();
	}

	@Then("^user sees the Branded experience page\\.$")
	public void user_sees_the_Branded_experience_page() throws Exception {
		assertTrue(beloginsuccesspage.verifyLoggedInonBEpage());
		String expected = "Offer Activated";
		String actual = beloginsuccesspage.clickActivateOffer();
		assertEquals(actual, expected);
	}

	@Given("^a user at Iframe page by using \"([^\"]*)\"$")
	public void a_user_at_Iframe_page_by_using(String arg1) throws Exception {
		MongoIframesCollection iframesColl = new MongoIframesCollection();
		List<Document> listDocument = iframesColl.getIframeMonogoId(arg1);
		Document doc = listDocument.get(0);
		String mongoId = doc.getObjectId("_id").toString();
		String arr[] = PageObject.withParameters(mongoId);
		iframeRegisterPage.open("openIframes", arr);
	}

	@When("^a user starts Signin process by using the Log In button on Iframe page$")
	public void a_user_starts_Signin_process_by_using_the_Log_In_button_on_Iframe_page() throws Exception {
		iframeHomePage.clickLoginLink();
	}

	@When("^enter email Address as \"([^\"]*)\" on  Iframe page$")
	public void enter_email_Address_as_on_Iframe_page(String arg1) throws Exception {
		iframeLoginPage.inputEmail(arg1);
	}

	@When("^enter password as \"([^\"]*)\" on  Iframe page$")
	public void enter_password_as_on_Iframe_page(String arg1) throws Exception {
		iframeLoginPage.inputPassword(arg1);
	}

	@When("^submits the request on  Iframe page$")
	public void submits_the_request_on_Iframe_page() throws Exception {
		iframeLoginPage.clickLogin();
	}

	@Then("^user sees the  Iframe page\\.$")
	public void user_sees_the_Iframe_page() throws Exception {
		assertTrue(iframesLoginSuccessPage.isLogoutLinkVisible());
		iframesLoginSuccessPage.clickLogout();
	}
}