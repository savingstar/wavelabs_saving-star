package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.bson.Document;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import common.AutoHelper;
import common.MongoBECollection;
import common.MongoIframesCollection;
import common.MongoOffersCollection;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.ClearCookiesPolicy;
import net.thucydides.core.annotations.Managed;
import pages.AlwaysOnSignUpPage;
import pages.BE_LandingPage;
import pages.BE_LoginSuccessPage;
import pages.BE_SignUpPage;
import pages.CouponsDetailsPage;
import pages.HomePage;
import pages.Iframes_HomePage;
import pages.Iframes_LoginPage;
import pages.Iframes_LoginSuccessPage;
import pages.Iframes_RegistrationPage;
import pages.MoreButtonDropDownPage;
import pages.SignInPopup;
import pages.SignUpPopup;
import pages.SingleCouponLandingPage;

public class SignUpStep {

	@Managed(clearCookies = ClearCookiesPolicy.BeforeEachTest)
	WebDriver driver;

	HomePage homePage;

	SignUpPopup signUpPopPage;

	SignInPopup signInPopPage;

	AlwaysOnSignUpPage alwaysOnSignUpPage;

	CouponsDetailsPage couponsDetailsPage;

	BE_LandingPage beLandingPage;

	BE_SignUpPage beSignupPage;

	BE_LoginSuccessPage beSuccessPage;

	Iframes_HomePage iframeHomePage;

	Iframes_RegistrationPage iframeRegisterPage;

	Iframes_LoginPage iframeLoginPage;

	Iframes_LoginSuccessPage iframesLoginSuccessPage;

	SingleCouponLandingPage singleCouponLandingPage;

	String firstName;

	String be_Email;

	String newEmailId;

	String beNewEmailId;

	String iframeNewEmailId;

	MoreButtonDropDownPage moreBtnDropDownPage;

	MongoOffersCollection offerscollection = new MongoOffersCollection();

	@Given("^a user is at Savingstar Home page$")
	public void a_user_is_at_Savingstar_Home_page() throws Exception {
		homePage.open();
	}

	@When("^user starts the signup process using Join Now$")
	public void user_starts_the_signup_process_using_Join_Now() throws Exception {
		homePage.clickJoinNowButton();
	}

	@When("^enter firstName as \"([^\"]*)\"$")
	public void enter_firstName_as(String arg1) throws Exception {
		firstName = arg1;
		signUpPopPage.setFirstName(arg1);
	}

	@When("^enter lastName as \"([^\"]*)\"$")
	public void enter_lastName_as(String arg1) throws Exception {
		signUpPopPage.setLastName(arg1);
	}

	@When("^enter zip code as \"([^\"]*)\"$")
	public void enter_zip_code_as(String arg1) throws Exception {
		String zipCode = String.valueOf(arg1);
		signUpPopPage.setZipCode(zipCode);
	}

	@When("^generate unique email Id$")
	public void generate_unique_email_Id() throws Exception {
		newEmailId = AutoHelper.getUniqueUserName("qa");
	}

	@When("^enter email address as generated from the above step$")
	public void enter_email_address_as_generated_from_the_above_step() throws Exception {
		signUpPopPage.setEmail(newEmailId);
	}

	@When("^enter email Address as \"([^\"]*)\"$")
	public void enter_email_Address_as(String arg1) throws Exception {
		signUpPopPage.setEmail(arg1);
	}

	@When("^enter password as \"([^\"]*)\"$")
	public void enter_password_as(String arg1) throws Exception {
		signUpPopPage.setPassword(arg1);
	}

	@When("^user starts the signup process using Join Now to Get This Rebate!$")
	public void user_starts_the_signup_process_using_Join_Now_to_Get_This_Rebate() throws Exception {
		// couponPopPage.clickJoinNow();
		singleCouponLandingPage.clickJoinNow();
	}

	@When("^starts the signup process using Join Saving star$")
	public void starts_the_signup_process_using_Join_Saving_star() throws Exception {
		signInPopPage.clickJoinSavingStarLink();
	}

	@Given("^a user at single landing page by using coupon title \"([^\"]*)\"$")
	public void a_user_at_single_landing_page_by_using_coupon_title(String arg1) throws Exception {
		List<Document> listDocument = offerscollection.getIdUsingOfferTitle(arg1);
		Document doc = listDocument.get(0);
		String mongoId = doc.getObjectId("_id").toString();
		String arr[] = PageObject.withParameters(mongoId);
		// couponPopPage.open("openSpecificOffer", arr);
		singleCouponLandingPage.open("openSpecificOffer", arr);
	}

	@Given("^submits the request for SignUp$")
	public void submits_the_request_for_SignUp() throws Exception {
		signUpPopPage.clickSignupEmailButton();
	}

	@Then("^user sees an validation message as \"([^\"]*)\"$")
	public void user_sees_an_validation_message_as(String arg1) throws Exception {
		String errorMessage = signUpPopPage.getErrorMessage();
		assertEquals("mismatch of error message", arg1, errorMessage);
	}

	@Then("^the Rebate is Activated$")
	public void the_Rebate_is_Activated() throws Exception {
		assertTrue("Registration Message is not visible it means account is not created",
				singleCouponLandingPage.isRebatedActivated());
		singleCouponLandingPage.clickSeeAllRebates();
	}

	@Then("^user is on always-on page$")
	public void user_is_on_always_on_page() throws Exception {
		assertTrue("Joining Text is not displayed", alwaysOnSignUpPage.joiningTxtIsVisible());
		String joiningTxt = alwaysOnSignUpPage.getJoiningText();
		String expectedTxt = "Thanks for joining, " + firstName + "!";
		assertEquals("Expected Text is not matched", joiningTxt, expectedTxt);
		alwaysOnSignUpPage.clickMayBeLaterLink();
	}

	@Then("^the account is created$")
	public void the_account_is_created() throws Exception {
		boolean isPresent = moreBtnDropDownPage.isHideMessagePresent();
		if (isPresent) {
			moreBtnDropDownPage.clickCloseHideMessage();
		}
		String result = moreBtnDropDownPage.verifySignInUser();
		Assert.assertEquals(newEmailId, result);
		moreBtnDropDownPage.clickActivatedTab();
		isPresent = moreBtnDropDownPage.isHideMessagePresent();
		if (isPresent) {
			moreBtnDropDownPage.clickCloseHideMessage();
		}
		Thread.sleep(2000);
		moreBtnDropDownPage.clickMoreLink();
		Thread.sleep(2000);
		moreBtnDropDownPage.SignOutPage();
		Thread.sleep(2000);
	}

	@Given("^a user at branded experience page by using \"([^\"]*)\"$")
	public void a_user_at_branded_experience_page_by_using(String arg1) throws Exception {
		MongoBECollection beColl = new MongoBECollection();
		List<Document> listDocument = beColl.getBrandedExperienceIdUsingName(arg1);
		Document doc = listDocument.get(0);
		String mongoId = doc.getObjectId("_id").toString();
		String arr[] = PageObject.withParameters(mongoId);
		beLandingPage.open("openBE", arr);
	}

	@When("^user activates on the offer$")
	public void user_activates_on_the_offer() throws Exception {
		beLandingPage.clickActivateYourOffer();
	}

	@When("^submits the BE Join request$")
	public void submits_the_BE_Join_request() throws Exception {
		beSignupPage.clickJoinButton();
	}

	@Then("^the Branded Experience account is created$")
	public void the_Branded_Experience_account_is_created() throws Exception {
		assertTrue("My Account is not visible", beSuccessPage.isMyAccountLinkVisible());
	}

	@Then("^the offer is in Activate state$")
	public void the_offer_is_in_Activate_state() throws Exception {
		assertTrue("Offer is not Activated", beSuccessPage.isOfferActivated());
		beSuccessPage.clickMyAccountDropDown();
		beSuccessPage.clickLogoutLink();
	}

	@When("^enter Branded Experience password as \"([^\"]*)\"$")
	public void enter_Branded_Experienc_password_as(String arg1) throws Exception {
		beSignupPage.inputPassword(arg1);
	}

	@When("^enter Branded Experience zip code as \"([^\"]*)\"$")
	public void enter_Branded_Experienc_zip_code_as(String arg1) throws Exception {
		beSignupPage.inputZipCode(arg1);
	}

	@When("^user starts signup process using Join SavingStar for Free Today$")
	public void user_starts_signup_process_using_Join_SavingStar_for_Free_Today() throws Exception {
		iframeHomePage.clickJoinSavingStarButton();
	}

	@When("^user starts signup process using Join SavingStar$")
	public void user_starts_signup_process_using_Join_SavingStar() throws Exception {
		iframeHomePage.clickJoinSavingStarLink();
	}

	@When("^user starts signup process using Login$")
	public void user_starts_signup_process_using_Login() throws Exception {
		iframeHomePage.clickLoginLink();
	}

	@When("^user starts signup using Register Today$")
	public void user_starts_signup_using_Register_Today() throws Exception {
		iframeLoginPage.clickRegisterToday();
	}

	@Given("^a user at Iframes page by using \"([^\"]*)\"$")
	public void a_user_at_Iframes_page_by_using_and(String arg1) throws Exception {
		MongoIframesCollection iframesColl = new MongoIframesCollection();
		List<Document> listDocument = iframesColl.getIframeMonogoId(arg1);
		Document doc = listDocument.get(0);
		String mongoId = doc.getObjectId("_id").toString();
		String arr[] = PageObject.withParameters(mongoId);
		iframeRegisterPage.open("openIframes", arr);
	}

	@When("^enter Iframe password as \"([^\"]*)\"$")
	public void enter_Iframe_password_as(String arg1) throws Exception {
		iframeRegisterPage.inputPassword(arg1);
	}

	@When("^submits the Create Account request$")
	public void submits_the_Create_Account_request() throws Exception {
		iframeRegisterPage.clickCreateAccount();
	}

	@Then("^Iframes account is created$")
	public void iframes_account_is_created() throws Exception {
		assertTrue("Registration Message is not visible it means account is not created",
				iframesLoginSuccessPage.isLoginSuccessMessagePresent());
		iframesLoginSuccessPage.clickLogout();
	}

	@When("^generate unique email Id for BE Signup$")
	public void generate_unique_email_Id_for_BE_Signup() throws Exception {
		beNewEmailId = AutoHelper.getUniqueUserName("qabe");
	}

	@When("^enter Branded Experience user email Address as generated from the above step$")
	public void enter_Branded_Experience_user_email_Address_as_generated_from_the_above_step() throws Exception {
		beSignupPage.inputEmailAddress(beNewEmailId);
	}

	@When("^generate unique email Id for Iframes Signup$")
	public void generate_unique_email_Id_for_Iframes_Signup() throws Exception {
		iframeNewEmailId = AutoHelper.getUniqueUserName("qaIframe");
	}

	@When("^enter Iframe user email Address as generated from the above step$")
	public void enter_Iframe_user_email_Address_as_generated_from_the_above_step() throws Exception {
		iframeRegisterPage.inputEmail(iframeNewEmailId);
	}

	@When("^enter Iframe emailConfirm as email Address$")
	public void enter_Iframe_emailConfirm_as_email_Address() throws Exception {
		iframeRegisterPage.inputEmailConfirm(iframeNewEmailId);
	}

	@When("^it is redirected to join savingstar$")
	public void it_is_redirected_to_sign_up_page() throws Exception {
	}

	@When("^user selects the coupon using \"([^\"]*)\"$")
	public void user_selects_the_coupon_using(String arg1) throws Exception {
		List<Document> listDocument = offerscollection.getIdUsingOfferTitle(arg1);
		Document doc = listDocument.get(0);
		String value = doc.getObjectId("_id").toString();
		couponsDetailsPage.clickOnCouponUsingHrefValue(value);
	}

	@When("^user starts the signup process using Sign In$")
	public void user_starts_the_signup_process_using_Sign_In() throws Exception {
		homePage.clickSignInButton();
	}

	@When("^user starts the signup process using Join Now on the snackbar$")
	public void user_starts_the_signup_process_using_Join_Now_on_the_snackbar() throws Exception {
		// homePage.evaluateJavascript("scroll(0, 250);");
		// WebElement joinButton=homePage.getJoinNowWebElementReference();
		// homePage.evaluateJavascript("arguments[0].scrollIntoView();", joinButton);
		homePage.evaluateJavascript("window.scrollTo(0, document.body.scrollHeight)");
		homePage.clickJoinNowOnSnackBar();
	}

	@And("^user starts signup process by using the join now to get rebate button from the Rebate pop up$")
	public void loggedoutJoin(){
		signUpPopPage.loggedoutJoin();
	}
}