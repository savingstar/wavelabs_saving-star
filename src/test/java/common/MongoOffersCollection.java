package common;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;

public class MongoOffersCollection {

	MongoClientWrapper mongoConnection = MongoClientWrapper.getInstance("savewave");

	//get coupon id of an rebate with status active from mongo DB
	public List<Document> getIdUsingOfferTitle(String title) {
		MongoCollection<Document> collection = mongoConnection.connecttoCollection("offers");
		BasicDBObject filters = new BasicDBObject();
		filters.append("title", title);
		filters.append("status", "published");
		filters.append("display_status", "active");
		List<Document> mongoIdList = collection.find(filters).projection(new BasicDBObject("_id", true))
				.into(new ArrayList<Document>());
		return mongoIdList;
	}

}
