package common;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class AutoHelper {

	public static String ranGen(int numLen) {
		final Random random = new Random();
		final StringBuffer str = new StringBuffer();
		final char[] digits = new char[numLen];
		digits[0] = (char) (random.nextInt(9) + '1');
		for (int i = 0; i < numLen; i++) {
			digits[i] = (char) (random.nextInt(10) + '0');
			str.append(digits[i]);
		}
		return str.toString();
	}

	/**
	 * getUniqueUserName - return a username that is guaranteed to be unique. Unlike
	 * random, which can generate repeats on occasion, using UUID & current time
	 * USecs will never repeat.
	 *
	 * @param isSWuser
	 *            - if the user is a SW user, '@savingstar.com' will be appended
	 * @param preFix
	 *            - a prefix that can identify a group of users for a particular
	 *            test
	 * @return userName - requested unique user name
	 */
	public static String getUniqueUserName(String preFix) {
		String userName = null;
		/*
		 * if (null != preFix) preFix += "_";
		 */
		String suffix = "@savingstar.com";
		final long lUnixSecs = new Date().getTime();
		final UUID uuid = UUID.randomUUID();
		final String sGuid = uuid.toString();
		final String[] sGuidParts = sGuid.split("-");
		// userName = preFix + sGuidParts[0] + "_" + lUnixSecs + suffix;
		userName = preFix + lUnixSecs + suffix;
		System.out.println("username is" + userName);
		return userName;
	}
}
