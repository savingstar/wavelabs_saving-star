package common;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;

public class MongoBECollection {

	MongoClientWrapper mongoConnection = MongoClientWrapper.getInstance("savewave");

	//get coupon id of a Branded Experience rebate with status active from mongo DB
	public List<Document> getBrandedExperienceIdUsingName(String beName) {
		MongoCollection<Document> collection = mongoConnection.connecttoCollection("branded_experiences");
		BasicDBObject filters = new BasicDBObject();
		filters.append("name", beName);
		filters.append("status", "published");
		List<Document> mongoIdList = collection.find(filters).projection(new BasicDBObject("_id", true))
				.into(new ArrayList<Document>());
		return mongoIdList;
	}
}
