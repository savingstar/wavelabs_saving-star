package common;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoClientWrapper {

	private static MongoClient mongoClient = null;
	static String stackValue = System.getProperty("stack");
	private static final String connectionUrl = "mongodb://mongo2.qa1.savewave.com:27017";

	private static MongoClientWrapper instanceOfMongoDB = null;
	private static String dbName = null;

	private MongoClientWrapper(String databaseName) {
		dbName = databaseName;
	}

	private static MongoClient getMongoClient(String clientUri) {
		if (mongoClient == null) {
			mongoClient = new MongoClient(new MongoClientURI(clientUri));
		}
		return mongoClient;

	}

	private static MongoDatabase getDB() {
		return getMongoClient(connectionUrl).getDatabase(dbName);
	}

	protected MongoCollection<Document> getUserCollection(String collectionName) {
		return getDB().getCollection(collectionName);
	}

	protected void closeConnection() {
		if (!(mongoClient == null)) {
			mongoClient.close();
		}
	}

	protected MongoCollection<Document> connecttoCollection(String collectionName) {
		return getUserCollection(collectionName);
	}

	//Singleton instance of MongoDB
	public static MongoClientWrapper getInstance(String databaseName) {
		if (instanceOfMongoDB == null) {
			synchronized (MongoClientWrapper.class) {
				if (instanceOfMongoDB == null) {
					instanceOfMongoDB = new MongoClientWrapper(databaseName);
				}
			}

		}
		return instanceOfMongoDB;
	}
	

}

