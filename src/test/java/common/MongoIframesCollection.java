package common;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;

public class MongoIframesCollection {

	MongoClientWrapper mongoConnection = MongoClientWrapper.getInstance("savewave");


	//get coupon id of a Iframe rebate with status active from mongo DB
	public List<Document> getIframeMonogoId(String iframeName) {
		MongoCollection<Document> collection = mongoConnection.connecttoCollection("iframes");
		System.out.println("collection" + collection);
		BasicDBObject filters = new BasicDBObject();
		filters.append("name", iframeName);
		filters.append("status", "active");
		System.out.println("filters" + filters);
		List<Document> mongoIdList = collection.find(filters).projection(new BasicDBObject("_id", true))
				.into(new ArrayList<Document>());
		
		return mongoIdList;
	}

}
